/* 
 * File:   bk-tree-container.h
 * Author: debian
 *
 * Created on December 9, 2014, 5:15 PM
 */


#ifndef BK_TREE_CONTAINER_H
#define	BK_TREE_CONTAINER_H

#include <vector>
#include <map>

#include <boost/serialization/serialization.hpp>
#include "threadpool.hpp"
#include <boost/smart_ptr.hpp>
#include <boost/bind.hpp>

#include <boost/thread/mutex.hpp>
#include "thread_pool_stl.h"

#include <chrono>
#include <thread>



namespace trees {
    
    
    namespace bktree_detail {
        
        // Helpers
boost::mutex m_io_monitor;


void print(std::string text)
{
  boost::mutex::scoped_lock lock(m_io_monitor);
  std::cout << text;
}

  boost::mutex _mutex;
  bool _found = false;
  int _workers = 0;
  int _run_number = 0;
  boost::condition_variable condition_;


        template<
            typename DataType,
            typename KeyType,
            typename MetricType
        >        
        std::vector<std::pair<KeyType, MetricType> > worker2(DataType * dt,KeyType key, MetricType mt,bool op, bool use_first_result) {
            
            return  dt->find_within(key, mt,  op, use_first_result);
        }
        
        
        template<
            typename DataType,
            typename KeyType,
            typename MetricType
        >        
        class worker {
            
            public: 
                //(*it, key,d,op,use_first_result)
                worker(DataType * _dt,KeyType _key, MetricType _mt,bool _op, bool _use_first_result, boost::threadpool::pool * _tp_ptr, int _run_number): dt(_dt),
                        key(_key),mt(_mt), op(_op), use_first_result(_use_first_result),tp_ptr(_tp_ptr),run_number(_run_number)
                {}
                ~worker() {}

	void run() {    
        
            
            if ( trees::bktree_detail::_found && run_number == trees::bktree_detail::_run_number) return ;
            
            /*
            std::ostringstream str;
            str<<"processing started "<<" with "<<key<<"\n";                                   
            str.flush();
            trees::bktree_detail::print(str.str());
       */
            
            //dt->settp(tp_ptr);
            result = dt->find_within(key, mt,  op, use_first_result);
            if (result.size()>0 && run_number == trees::bktree_detail::_run_number)       {
                
              /*          std::ostringstream str;
                        str<<"found for "<<key <<"\n";     
                        str.flush();
                        trees::bktree_detail::print(str.str());  
                */
               boost::mutex::scoped_lock lock(_mutex);
              trees::bktree_detail::_found =true;
              tp_ptr->clear(); 
              condition_.notify_all(); 
              
            }
            
            if (run_number == trees::bktree_detail::_run_number) {
             boost::mutex::scoped_lock lock(_mutex);              
            trees::bktree_detail::_workers--;
            condition_.notify_all(); 
            }
            
            
            /*
            std::ostringstream str2;
            str2<<"workers  "<<trees::bktree_detail::_workers<<" with "<<key<<"\n";                                   
            str2.flush();
            trees::bktree_detail::print(str2.str());
            */
    
             
              
            /*
            str.clear();
            str<<"processing finished "<<" with "<<key<<"\n";                                   
            str.flush();
            trees::bktree_detail::print(str.str());
           */
        
        }
        
        std::vector<std::pair<KeyType, MetricType> > get()  { return result;}
               
            private:
               DataType * dt;
               KeyType  key;
               MetricType  mt;
               bool op;
               bool use_first_result;
               std::vector<std::pair<KeyType, MetricType> >  result;
               boost::threadpool::pool * tp_ptr;
               int run_number;

        };
        
        
        

        
        
    }
    
    
    	template <
            typename DataType,
            typename KeyType,
            typename MetricType
	> 
        class bk_tree_container {

            public:
                bk_tree_container(unsigned int maxsize=50000):_maxsize(maxsize),_cursize(0) { 
                    _container.push_back(new DataType());
                }
                ~bk_tree_container() { 
                    std::for_each(_container.begin(), _container.end(), 
                        [](DataType * &p) { 
                        if (p != nullptr) delete p;    
                        });                
                }
                
		void insert(const KeyType &key) {
                
                    if (_cursize>_maxsize) {
                         _container.push_back(new DataType());
                         _cursize = 0;
                    }
                    
                    _container.back()->insert(key);
                    _cursize++;
                
                }
                
                
//                std::vector<std::pair<KeyType, MetricType> > find_within(KeyType key, MetricType d, bool op = true, bool use_first_result=false) const {
  //std::vector<std::pair<KeyType, MetricType> > find_within(thread_pool_stl * pool, KeyType key, MetricType d, bool op = true, bool use_first_result=false) const {
//                std::vector<std::pair<KeyType, MetricType> > find_within( boost::threadpool::pool * tp_ptr,boost::threadpool::pool * tp_ptr2, KeyType key, MetricType d, bool op = true, bool use_first_result=false) const {
      std::vector<std::pair<KeyType, MetricType> > find_within( boost::threadpool::pool * tp_ptr, KeyType key, MetricType d, bool op = true, bool use_first_result=false) const {
                    /*
                    typedef std::shared_ptr<trees::bktree_detail::worker<DataType,KeyType,MetricType> > workerPtr;
                    std::vector<workerPtr> results;
                    //the begin of the parallelization                                   
                    
                     {
//                        thread_pool_stl pool(_container.size() );
                         thread_pool_stl pool(8);
                         for (auto it =  _container.begin();it!=_container.end();it++) {                            
                            workerPtr job (new trees::bktree_detail::worker<DataType,KeyType,MetricType>(*it, key,d,op,use_first_result));    
                            pool.async(std::function<void()>(std::bind(&trees::bktree_detail::worker<DataType,KeyType,MetricType>::run,job)));        
                            results.push_back(job);
                         }
                     }
                     
                    //the end of the parallelization                                   
                    */
                    
//                    thread_pool_stl pool(5);
//                    pool.noJoin(true);
      
/*                    pool->cleartasks(true);
                    typedef std::vector<std::pair<KeyType, MetricType> > ResType;
                    std::vector<std::future<ResType>> f;
                    
                    for (auto it =  _container.begin();it!=_container.end();it++) {  
                        
                         f.emplace_back(pool->async(std::function<ResType()>(std::bind(trees::bktree_detail::worker2<DataType,KeyType,MetricType>, *it, key,d,op,use_first_result))));
                                                 
                         }
                     for (auto it = f.begin(); it != f.end(); it++) {
                         auto res = it->get();
                         if (res.size() >0 ) {
//                             try {
//                                pool.emergency_close();                                 
//                             } catch (...) { }
                             
                             return res;
                         }
                        }

*/                    
                    typedef boost::shared_ptr<trees::bktree_detail::worker<DataType,KeyType,MetricType> > workerPtr;
                    std::vector<workerPtr> results;
                    
                    //the begin of the parallelization                                   
                    /*
                     {
                        boost::threadpool::pool tp; // tp is handle to the pool
                        tp.size_controller().resize(_container.size());
                        for (auto it =  _container.begin();it!=_container.end();it++) {
                            workerPtr job (new trees::bktree_detail::worker<DataType,KeyType,MetricType>(*it, key,d,op,use_first_result));    
                             boost::threadpool::schedule(tp, boost::bind(&trees::bktree_detail::worker<DataType,KeyType,MetricType>::run, job));
                             results.push_back(job);
                        }
                        
                    } //the end of the parallelization                                   
                    */
                    
    //                    boost::threadpool::pool * tp_ptr = new boost::threadpool::pool(8);
                    tp_ptr->clear();
//                    std::ostringstream str;                    
//                    str<<"our size "<<tp_ptr->size()<<"\n";
//                    str.flush();
//                     trees::bktree_detail::print(str.str());       
                    
                    //tp_ptr->size_controller().resize(_container.size());
                    //tp_ptr->size_controller().resize(5);
                    trees::bktree_detail::_run_number++;
                    trees::bktree_detail::_found = false;
                    trees::bktree_detail::_workers = 0;
                    
                    for (auto it =  _container.begin();it!=_container.end();it++) {
                        
/*                        std::ostringstream str;
                        str<<"the size "<< (*it)->size() <<"\n";     
                        str.flush();
                        trees::bktree_detail::print(str.str());    
*/                        
                               if (trees::bktree_detail::_found)  {
/*                                   std::ostringstream str;
                                    str<<"break\n";  
                                    str.flush();
                                    trees::bktree_detail::print(str.str());    
 */
                                   break;
                                   
                               }
                            //workerPtr job (new trees::bktree_detail::worker<DataType,KeyType,MetricType>(*it, key,d,op,use_first_result,tp_ptr2));    
//                            workerPtr job (new trees::bktree_detail::worker<DataType,KeyType,MetricType>(*it, key,d,op,use_first_result));    
                              workerPtr job (new trees::bktree_detail::worker<DataType,KeyType,MetricType>(*it, key,d,op,use_first_result,tp_ptr,trees::bktree_detail::_run_number));    
                            tp_ptr->schedule(boost::bind(&trees::bktree_detail::worker<DataType,KeyType,MetricType>::run, job));
//                            boost::threadpool::schedule(tp_ptr,boost::bind(&trees::bktree_detail::worker<DataType,KeyType,MetricType>::run, job));
                            results.push_back(job);
                            trees::bktree_detail::_workers++;

                            //job->run();
/*                                std::vector<std::pair<KeyType, MetricType> > result = job->get();
                                if (result.size()>0) {
                    
                                    return result; 
                            }
                                */
                        }
                    /*
                       std::ostringstream str;
                       str<<"# workers  "<< trees::bktree_detail::_workers<<"\n";    
                       str.flush();
                       trees::bktree_detail::print(str.str());   
                       */
                    if (!tp_ptr->empty()) {
//                        std::ostringstream str;
//                        str<<"we should wait "<<tp_ptr->active()<<"\n";                                   
//                        str<<tp_ptr->pending()<<"\n";
//                        str<<_container.size()<<"\n";
//                        str<<tp_ptr->size()<<"\n";
                        
//                        if (tp_ptr->size() == 0)  {
//                            bool resize_res = tp_ptr->size_controller().resize(8);
//                             str<<"result of resize "<<resize_res<<"\n";
//                            str<<tp_ptr->size()<<"\n";
//                        }
//                        str.flush();
//                        trees::bktree_detail::print(str.str());    
//                        tp_ptr->clear();
                        if (!trees::bktree_detail::_found) tp_ptr->wait(); 
                    } //else std::cout<<"we should not wait \n";
 
//                    std::ostringstream str;
//                    str<<trees::bktree_detail::_found<<"\n"; 
//                    tp_ptr->wait();
/*
                    boost::unique_lock< boost::mutex > lock( trees::bktree_detail::_mutex );
                    while ( !(trees::bktree_detail::_found || trees::bktree_detail::_workers == 0) )
                    {

                    trees::bktree_detail::condition_.wait( lock );


                    }
                    lock.unlock();
                   
        */                    
//                    while (!trees::bktree_detail::_found) { int i; }
//                    str<<trees::bktree_detail::_found<<"\n"; 
//                    str.flush();
//                    trees::bktree_detail::print(str.str());    
                    
/*
                        str.clear();
                        str<<"before delete "<<"\n";                                   
                        str<<tp_ptr->active()<<"\n";
                        str<<tp_ptr->pending()<<"\n";
                        str.flush();
                        trees::bktree_detail::print(str.str());       
*/                                        
//                        delete tp_ptr;
/*                        str.clear();
                        str<<"go further \n";
                        str.flush();
                        trees::bktree_detail::print(str.str());       
*/ 
 
                    
                    for (auto it=results.begin();it!=results.end();it++ ) {
                        std::vector<std::pair<KeyType, MetricType> > result = (*it)->get();
                        if (result.size()>0) {
                    
                            return result; 
                        }
                    
                    }
                     
                    
                    return std::vector<std::pair<KeyType, MetricType> >();
                    
                }
                
                bool remove( const KeyType &key) {return true;};
                
            private:
                
                unsigned int _maxsize;
                unsigned int _cursize;
              
              
                std::vector<DataType *> _container;
                
                
                
            
                
                    
        };
    
    
}

#endif	/* BK_TREE_CONTAINER_H */

