
/****h* BK-TREE/bktree
  *  NAME
  *    bktree   realization of the BK-tree
  *  
  *  COPYRIGHT
  *    "(c) 2014 by <Unister Gmbh/Igor Marfin>" 
  *  
  *  SYNOPSIS
  *    
  *
  *
  *  DESCRIPTION
  *    * definition of the node
  *    * definition of the tree
  *    * Estimate timing of the search  of entries in BK-Tree and Cayley processing
  *
  *  INPUTS
  *
  *  RESULT
  *
  *  EXAMPLE
  *
  *  NOTES
  *    
  *  BUGS
  *  SEE ALSO
  *    BK-TREE/httpserver
  *  |html <img src="figs/classtrees_1_1bktree__coll__graph.png"> 
  ******
  * You can use this space for remarks that should not be included
  * in the documentation.
  */


#ifndef _BKTREE_MT_HPP_
#define _BKTREE_MT_HPP_


#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <cmath>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>


#include "tools/tools-text.hpp"
#include "threadpool.hpp"
#include <boost/smart_ptr.hpp>
#include <boost/bind.hpp>
#include "thread_pool_asio.h"



/****** BK-TREE/bktree/boost::serialization
 * DESCRIPTION 
 *    inclusion of the boost::serialization
 * SOURCE
*/


#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>

/******/

/****** BK-TREE/bktree/Levenshtein
 * DESCRIPTION 
 *    Levenshtein Distance Algorithm
 * SOURCE
*/


#include "levenshtein-distance.hpp"
#include <numeric>

/******/


// Helpers
boost::mutex m_io_monitor;


void print(std::string text)
{
  boost::mutex::scoped_lock lock(m_io_monitor);
  std::cout << text;
}



namespace trees {

namespace bktree_detail {

    boost::mutex mutex;


/****c* BK-TREE/bktree/trees::bktree_detail::tree_node
 * DESCRIPTION 
 *  A node definition for the bktree
 * |html <img src="figs/classtrees_1_1bktree__detail_1_1tree__node__coll__graph.png">
 * SOURCE
*/


template <typename KeyType, typename MetricType, typename Distance>
class tree_node_mt
{
private:
	typedef tree_node_mt<KeyType, MetricType, Distance> NodeType;
	
private:
	KeyType value;
	std::map<MetricType, NodeType *> *children;

public:
	tree_node_mt(const KeyType &key)
		: value(key), children(NULL),counter(1) { }
	tree_node_mt()
                : value(""), children(NULL),counter(1) { }


	~tree_node_mt() {
		if (children) {
			for (auto iter = children->begin(); iter != children->end(); ++iter)
				delete iter->second;
			delete children;
		}
	}


private:
	friend class boost::serialization::access;
	template<class Archive>
                void serialize(Archive & ar, const unsigned int version)
                {
			ar &  children;
			ar &  value;
			ar &  counter;
                }



public: 
		KeyType getVal() const  { return value;}
		int counter; //to store the number of the different entries with the same key. It is needed by deletion.


	bool insert(NodeType *node) {
		if (!node)
			return false;



		Distance d;
		MetricType distance = d(node->value, this->value);
		if (distance < 1e-6) {
		    this->counter++;
		return false; /* value already exists */
		}


		if (!children)
			children = new std::map<MetricType, NodeType *>();

		auto iterator = children->find(distance);
		if (iterator == children->end()) {
			children->insert(std::make_pair(distance, node));
			return true;
		}

		return iterator->second->insert(node);
	}



public: 
        
          bool remove(const KeyType &key)
{
		std::vector<NodeType *> a = _find2erase(key,this);
                if (a.size()>1) {
			Distance d;
	                MetricType distance = d(a[1]->getVal(), this->value);
			if (distance == 0) return true;
			distance = d(a[0]->getVal(), a[1]->getVal());
                        a[0]->children->erase(distance);
			if (a[1]->has_children())
	                for (auto iter = a[1]->children->begin(); iter != a[1]->children->end(); ++iter)  			
			{		
	        	        insert(new NodeType(iter->second->getVal()));
			}

			 delete a[1];
			return true;
		}

		return false;
}

protected:
      std::vector<NodeType *> _find2erase(const KeyType &key, NodeType * prev)
	{
                Distance d;
                MetricType distance = d(key, this->value);
                if (distance < 1e-6) {
                        std::vector<NodeType *>a;
                        this->counter--;
                         std::cout<<"bk-tree  to delete    ::"<<this->value <<"::"<<this->counter<<"\n";
//                    if (key == this->value) {
                         if (this->counter == 0) {
                            a.push_back(prev); a.push_back(this);
                               std::cout<<"bk-tree  delete    ::"<<this->value <<"::"<<"\n";
                         } else   std::cout<<"bk-tree cannot delete because it's doubled   ::"<< this->value <<"::"<<"\n";
                        return a;
//                    } else return a; // return empty in order preserve a node from removing

                }

		if (!children) { return std::vector<NodeType *>();}

                auto iterator = children->find(distance);
		if (iterator != children->end())   {  return iterator->second->_find2erase(key,this); }
                else { return std::vector<NodeType *>();}
	}



protected:
	bool has_children() const {
		return this->children && this->children->size();
	}


protected:

    // to test: as a step of bk-tree optimization of the search
	//void _find_within(std::vector<std::pair<KeyType, MetricType> > &result, const KeyType &key, MetricType d, bool op, bool use_first_result ) const {
	bool _find_within(std::vector<std::pair<KeyType, MetricType> > &result, const KeyType &key, MetricType d, bool op, bool use_first_result ) const {


 if (use_first_result && result.size()>0) return true ;

	    Distance f;
		MetricType n = f(key, this->value);
                if (op) {
	          	if (n <= d) 
				result.push_back(std::make_pair(this->value, n)); 
		} else {
			if (n <= d) {
                                result.push_back(std::make_pair(this->value, 1-n));
			}
		}

    // to test: as a step of bk-tree optimization of the search
	//if (use_first_result && result.size()>0) return;
    if (use_first_result && result.size()>0) return true ;


		if (!this->has_children())
// to test: as a step of bk-tree optimization of the search
//			return;
        return false;

		for (auto iter = children->begin(); iter != children->end(); ++iter) {
			MetricType distance = iter->first;
			if (n - d <= distance && distance <= n + d) {
                            
// to test: as a step of bk-tree optimization of the search
//				iter->second->_find_within(result, key, d,op,use_first_result);
                        bool boolres=iter->second->_find_within(result, key, d,op,use_first_result);
                        if (boolres) return true;
                    }

		}


// to test: as a step of bk-tree optimization of the search
//        return;
        return false;
	}

         // to test: as a step of bk-tree optimization of the search
	//void _find_within(std::vector<std::pair<KeyType, MetricType> > &result, const KeyType &key, MetricType d, bool op, bool use_first_result ) const {
	void _find_within_mt(std::vector<std::pair<KeyType, MetricType> > &result, const KeyType &key, MetricType d, bool op, bool use_first_result,boost::threadpool::pool *_tp_ptr ) const {



 if (_tp_ptr == nullptr) return  ;

  if (use_first_result && result.size()>0)  {
        boost::mutex::scoped_lock lock(mutex);
      _tp_ptr->clear();
     return  ; 
 }

 
 /*
 std::ostringstream str;
            
      
 
 str<<"I am "<<this->value<<"\n";
 str<<"use_first_result "<<(int)use_first_result<<"\n";
 str<<"Looking for "<<key<<"\n";
 */
	    Distance f;
		MetricType n = f(key, this->value);
                if (op) {
	          	if (n <= d) {
                                boost::mutex::scoped_lock lock(mutex);
				result.push_back(std::make_pair(this->value, n)); 
                                lock.unlock();
                        }
		} else {
			if (n <= d) {
                            boost::mutex::scoped_lock lock(mutex);
                                result.push_back(std::make_pair(this->value, 1-n));
                                lock.unlock();
			}
		}
/*
str<<"n "<<n<<" with "<<this->value<<" "<<key<<"\n";                
str<<"d "<<d<<" with "<<this->value<<" "<<key<<"\n";       
str<<"size of the result is "<<result.size()<<"\n";       
      str.flush();
       print(str.str());
       */
       
    // to test: as a step of bk-tree optimization of the search
	//if (use_first_result && result.size()>0) return;
    if (use_first_result && result.size()>0) {
        boost::mutex::scoped_lock lock(mutex);
        _tp_ptr->clear();
        return  ;
    }
/*
       std::ostringstream str2;
       
                str2<<"Going further  \n";
str2.flush();
       print(str2.str());
  */
		if (!this->has_children())
// to test: as a step of bk-tree optimization of the search
			return;
/*
        std::ostringstream str3;
 str3<<"Going further  for childrens \n";
   str3.flush();
       print(str3.str());
       */
       
		for (auto iter = children->begin(); iter != children->end(); ++iter) {
			MetricType distance = iter->first;
			if (n - d <= distance && distance <= n + d) {
                            
// to test: as a step of bk-tree optimization of the search
                            _tp_ptr->schedule(boost::bind(&tree_node_mt::_find_within_mt, iter->second,boost::ref(result), key, d,op,use_first_result,_tp_ptr));


                    }

		}


// to test: as a step of bk-tree optimization of the search
        return;
	}


public:

	std::vector<std::pair<KeyType, MetricType> > find_within(const KeyType &key, MetricType d, bool op,bool use_first_result) const {
		std::vector<std::pair<KeyType, MetricType> > result;


		_find_within(result, key, d,op,use_first_result);


		return result;
	}

       	std::vector<std::pair<KeyType, MetricType> > find_within_mt(const KeyType &key, MetricType d, bool op,bool use_first_result, boost::threadpool::pool *_tp_ptr) const {

                std::vector<std::pair<KeyType, MetricType> > result;

                if (_tp_ptr != nullptr) {
//                    std::cout<<"let'start\n";

                    _tp_ptr->clear();
                     _find_within_mt(result, key, d,op,use_first_result,_tp_ptr);
                        

                    if (!_tp_ptr->empty()) {
//                        std::cout<<"_tp_ptr is not empty, wait\n";
                        _tp_ptr->wait(); 
                    } 
                                         
//                    delete ptrPool;
                }
                    //std::cout<<"result has "<<result.size()<<" elements\n";

		return result;
	}



public:
	void dump_tree(int depth = 0) {
		for (int i = 0; i < depth; ++i)
			std::cout << "    ";
		std::cout << this->value << std::endl;
		if (this->has_children())
			for (auto iter = children->begin(); iter != children->end(); ++iter) {
				std::cout << " children with depth " << depth << " of " << this->value << " (distance: "<< iter->first  <<")";
				iter->second->dump_tree(depth + 1);
			}
	}




}; /* tree_node_mt */
/******/

/****c* BK-TREE/bktree/trees::bktree
 * DESCRIPTION 
 *   bktree definition
 * |html <img src="figs/classtrees_1_1bktree__coll__graph.png">
 * SOURCE
*/

	template <
		typename KeyType,
		typename MetricType
	>
	struct default_distance_mt
	{
		MetricType operator()(const KeyType &ki, const KeyType &kj) {
			return sqrt((ki - kj) * (ki - kj));
		}
	};



} /* namespace bktree_detail */



	template <
		typename KeyType,
		typename MetricType = double,
		typename Distance = bktree_detail::default_distance_mt<KeyType, MetricType>
	>
	class bktree_mt
	{
	private:
		typedef bktree_detail::tree_node_mt<KeyType, MetricType, Distance> NodeType;
		NodeType *m_top;
		size_t m_n_nodes;
                boost::threadpool::pool *tp_ptr;

	public:
		bktree_mt() : m_top(NULL), m_n_nodes(0),tp_ptr(nullptr) { }
                void settp(boost::threadpool::pool *_tp_ptr) { tp_ptr = _tp_ptr; }


	private:
        	friend class boost::serialization::access;
	        template<class Archive>
        	void serialize(Archive & ar, const unsigned int version)
                {
                        ar &  m_top; 
                        ar & m_n_nodes; 
                }



	public:
		void insert(const KeyType &key) {
			NodeType *node = new NodeType(key);
			if (!m_top) {
				m_top = node;
				m_n_nodes = 1;
				return;
			}
			if (m_top->insert(node))	++m_n_nodes;
			else delete node;

			return;
		}

		bool remove( const KeyType &key)
		{	
			if (!m_top) return false;
			else {

				if (m_top->remove(key)) --m_n_nodes;
                                else return false;

			}

			return true;
		}

		std::vector<std::pair<KeyType, MetricType>> find_within(KeyType key, MetricType d, bool op = true, bool use_first_result=false) const {

                                            
                                        std::vector<std::pair<KeyType, MetricType>>  res;
                                        
                                        if (tp_ptr != nullptr) res = m_top->find_within_mt(key, d, op,use_first_result,tp_ptr);
                                        else res=m_top->find_within(key, d, op,use_first_result);
                    			return res;
                    
		}

		size_t size() const {
			return m_n_nodes;
		}


		void dump_tree() {
			m_top->dump_tree();
		}



};  /*  bktree_mt  */
/******/

} /* namespace trees */




#endif
