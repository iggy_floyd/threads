/* 
 * File:   bktree-container.h
 * Author: debian
 *
 * Created on December 11, 2014, 10:08 AM
 */

#ifndef BKTREE_CONTAINER_H
#define	BKTREE_CONTAINER_H

// serialization support
#include <boost/serialization/serialization.hpp>


// worker support
#include <boost/smart_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/thread/mutex.hpp>


// thread pool support
#include "threadpool.hpp"


// for internal storage
#include <map>

// for testing
#include <chrono>
#include <thread>
#include "tools/cpu-time.hpp"

// this value defines the speeding of search and remove operations:

// for an example, with  8 threads one needs  on average 230 ms 
// for searching in 1M connected properties with internal buffer of 15 entries
#define MAXTHREADS 8


namespace trees {
    
    
     namespace bktree_detail {
        
     
         // helpers
        boost::mutex m_io_monitor;

        void print(std::string text) {
            boost::mutex::scoped_lock lock(m_io_monitor);
            std::cout << text;
        } 
        
        boost::mutex mutex_remove;   
 
        
      
         template<
            typename DataType,
            typename KeyType,
            typename MetricType
        >        
        class worker {
            
            public: 
                typedef std::vector<std::pair<KeyType, MetricType> >  WorkerReturnType; 
             
            public: 
            
                worker(DataType * _dt,KeyType _key, MetricType _mt,bool _op, bool _use_first_result): dt(_dt),
                        key(_key),mt(_mt), op(_op), use_first_result(_use_first_result)
                {}
                ~worker() {}
                
                

                void run() {    
// for testing purpose
                    latency = tools::get_wall_time()-latency;

//                    std::cout<<"Starting a job on ..."<<key<<"\n";
                    try {
                        result = dt->find_within(key, mt,  op, use_first_result);
                    } catch (...) {  fail = true; }
                    
                    ready = true;    
                    
//                    std::cout<<"Finishing a job on ..."<<key<<" with found "<<result.size()<<" connected entries"<<"\n";
                }
                
                // makes a remove of the entry
                void remove() {
                    try {
                        //{
                            
                            boost::unique_lock< boost::mutex > lock( trees::bktree_detail::mutex_remove );                            
//                            std::cout<<"!!!Starting a job on ..."<<key<<"\n";
                            dt->remove(key);
//                            std::cout << "!!!finished the lock..."<<key<<"\n";
                            lock.unlock();
                        //}
                        
                    } catch (...) { }
                                            
                    ready = true;  
                    remover = true;
                }
                
        
                WorkerReturnType getResult()  { return result;}
                KeyType getKey()  { return key;}
                
                bool isFail () const { return fail;}
                bool isReady () const { return ready;}
                bool isRemover () const { return remover;}
                
                
                
         
         
            private:
                DataType * dt;
                KeyType  key;
                MetricType  mt;
                bool op;
                bool use_first_result;
                WorkerReturnType result;
                bool ready = false;
                bool fail = false;
                bool remover = false;
            public:
                double latency = tools::get_wall_time();
                
                 
               
               

        };
        
        
        
        
        
            
     }    // end of bktree_detail namespace
     
     
     
        
        /* bktree_container class definition */
        
        template <
            typename DataType,
            typename KeyType,
            typename MetricType
	> 
        class bktree_container {

            public:
                typedef boost::shared_ptr<trees::bktree_detail::worker<DataType,KeyType,MetricType> > workerPtr;
                typedef typename trees::bktree_detail::worker<DataType,KeyType,MetricType>::WorkerReturnType WorkerReturnType;
                typedef  std::multimap<KeyType,WorkerReturnType>  ContainerResultType;

            
            public:
                bktree_container(unsigned int maxsizethreads=10):_maxsizethreads(maxsizethreads){ 
                 
                    _container = new DataType();
                    _tp_ptr= new boost::threadpool::pool(MAXTHREADS);
                    
                    
                }
                
                
                ~bktree_container() { 
                    
                    if (_container != nullptr) delete _container;
                    if (_tp_ptr != nullptr) delete _tp_ptr;
                } 
                

		void insert(const KeyType &key) {
                
                    
                    _container->insert(key);                    
                
                }
                    
                
		void randomization() {
                
                    
                    _container->randomization();                    
                
                }
                
                // remove can be done with locking only. Worker utilizes a locking section
                bool remove( const KeyType &key) {
                

                    // we have to wait until all searcher finish
                    if (!is_remover) {
                        _tp_ptr->wait();
                    }
                    is_remover = true;
                    
                    volatile unsigned int pending = _tp_ptr->pending(); // how many pending tasks are waiting to be executed
                    
                    // wait for a while until  the collection of  pending tasks  is being reduced to the acceptable level.
                    // this is a protection against the overloading
                    while (pending > _maxsizethreads) {
                        pending = _tp_ptr->pending(); 
                    }
                    
                     workerPtr job (new trees::bktree_detail::worker<DataType,KeyType,MetricType>(_container, key,0.,false,true));    
                    _tp_ptr->schedule(boost::bind(&trees::bktree_detail::worker<DataType,KeyType,MetricType>::remove, job));                                        
                    _worker_results.push_back(job); //save the job and future results of its calculation 
                  
                    //return _container->remove(key);
                    
                    return true;
                
                };
                
                
                // performs an asynchronous search in the bk-tree
                void find_within( KeyType key, MetricType d, bool op = true, bool use_first_result=false)  {
                
                      // we have to wait until all removers finish
                    if (is_remover) {
                        _tp_ptr->wait();
                    }
                    is_remover = false;
                    
                   volatile unsigned int pending = _tp_ptr->pending(); // how many pending tasks are waiting to be executed
                    
                    // wait for a while until  the collection of  pending tasks  is being reduced to the acceptable level.
                    // this is a protection against the overloading
                    while (pending > _maxsizethreads) {
                        pending = _tp_ptr->pending(); 
                    }
                    
                    
                    
                    // add a new search job 
                    //std::cout<<"Adding a new job\n";
                    workerPtr job (new trees::bktree_detail::worker<DataType,KeyType,MetricType>(_container, key,d,op,use_first_result));    
                    _tp_ptr->schedule(boost::bind(&trees::bktree_detail::worker<DataType,KeyType,MetricType>::run, job));                                        
                    _worker_results.push_back(job); //save the job and future results of its calculation 
                    
                }
                
                
                
                // these two functions are used only for testing purpose: should be removed in the release version
                
                // could be run on a time-to-time basis: with a long period between calls
                void clean() {
                    
                    // clean the temporary job vector:
                    // if a job is finished (ready), save its result and remove the job from the vector
                    _worker_results.erase(                     
                                
                        std::remove_if(
                                _worker_results.begin(),
                                _worker_results.end(),
                                [this] ( workerPtr &p) mutable {
                                    
                                   /*
                                    std::cout<<"Cleaning a job\n";
                                    std::cout<<p->getKey()<<"\n";
                                    std::cout<<p->isReady()<<"\n";
                                    std::cout<<p->isFail()<<"\n";
                                    
                                            */ 
                                    
                                    if ( p->isReady() ) {
                                        if (!p->isRemover()) {
                                            latency_time.push_back(p->latency);
                                            this->_results.insert(std::pair<KeyType,WorkerReturnType>(p->getKey(),p->getResult()));
                                        }
                                        return true;
                                    }
                                    else if ( p->isFail() ) {                                      
                                      return true;
                                    } else return false;
                                    
                                }                    
                        ),
                         _worker_results.end()   
                         
                    );                    
                                        
                    
                }
                
                
                // the method might be called in the end to process the results
                ContainerResultType getResults() { return _results;}
                 std::vector<double> getLatency() { return latency_time;}
                
                
                // a few flags to tell if there are  tasks to be completed
                                
                size_t active() const { return _tp_ptr->active(); }

                size_t pending() const { return _tp_ptr->pending(); }
                
                bool empty() const { return _tp_ptr->empty(); }
                
                void wait() { _tp_ptr->wait(); }
                
                size_t size() const { return _container->size(); }

                bool isRemove() const { return is_remover;}

            private:
                
                bool is_remover = false;
                DataType * _container;
                unsigned int _maxsizethreads;
                boost::threadpool::pool * _tp_ptr;
                
                    
                // these containers are used only for testing purpose: should be removed in the release version
                std::vector<workerPtr> _worker_results;
                
                ContainerResultType _results;
                
                std::vector<double> latency_time; // to store the latency  of the tasks
                
        };
        
  
} // end of trees namespace

    
    
    
    
    
    
    
    
    
    



#endif	/* BKTREE_CONTAINER_H */

