#ifndef LEVENSHTEINMOD_H
#define	LEVENSHTEINMOD_H

#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <fstream>           



#include "tools/tools-text.hpp"
#include "tools/vprintf.hpp"



/****h* BK-TREE/levenshtein-distance
  *  NAME
  *    levenshtein-distance
  *  
  *  COPYRIGHT
  *    "(c) 2014 by <Unister Gmbh/Igor Marfin>" 
  *  
  *  SYNOPSIS
  *    
  *
  *
  *  DESCRIPTION
  *     calculates the Levenshtein Distance (LD)
  *    * "original" LD (see  http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance)
  *    * or so called similarity based on the "inverse LD"
  *
  *  INPUTS
  *
  *  RESULT
  *
  *  EXAMPLE
  *
  *  NOTES
  *    
  *  BUGS
  *  SEE ALSO
  *    BK-TREE/httpserver
  ******
  * You can use this space for remarks that should not be included
  * in the documentation.
  */


#include <chrono>
#include <thread>



template<class T>
T prepare_field_mod( const T &s11,int what_world_to_compare)
{


                
         const T delim("|||");
         return tools::boostsplit<T>(s11, delim)[0];
}



template<class T>
unsigned int levenshtein_distance_mod( T &s1, T & s2, double search_distance, /* T &filename,*/ bool mod) {
    
        if (s1.size()<s2.size()) s2.swap(s1);
	const size_t len1 = s1.size(), len2 = s2.size();
	std::vector<unsigned int> col(len2+1), prevCol(len2+1);

        //std::ofstream outFile;         
        //outFile.open(filename.c_str());        // Step #3 - Open outFile.

        
	
        
	for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
	for (unsigned int i = 0; i < len1; i++) {
		col[0] = i+1;
		for (unsigned int j = 0; j < len2; j++)
			col[j+1] = std::min( std::min(prevCol[1 + j] + 1, col[j] + 1),
								prevCol[j] + (s1[i]==s2[j] ? 0 : 1) );
                
                
		//std::cout<<"i ="<<i<<"; col[len2-1] = "<<((double)col[len2-1])/len2<<"; col[len2] = "<<((double)col[len2])/len2<<" "<<col[len2]<<"\n";
		col.swap(prevCol);
                
		if (mod) {
                    double y0 = ((double)prevCol[len2])/len2;
                    if (y0<=search_distance) {
                        //std::cout<<"distance reach\n";
                            return  prevCol[len2];
                    }
                    
                    double x0 = (len1 > 1)?((double)i)/(len1-1):(double)i;
                    
                    if ((y0-search_distance)+x0 >1.) {
                        //std::cout<<"distance can't reach: "<< (y0-search_distance)+x0 <<"\n";
                        return prevCol[len2];}
                }

		//std::cout<<"prevCol[len2-1] = "<<((double)prevCol[len2-1])/len2<<"; prevCol[len2] = "<<((double)prevCol[len2])/len2<<"\n";
                //if (!outFile.fail() && len1 > 1) outFile<<((double)i)/(len1-1)<<" "<<((double)prevCol[len2])/len2<<"\n";
                //if (!outFile.fail() && len1 == 1) outFile<<((double)i)<<" "<<((double)prevCol[len2])/len2<<"\n";
	}
         //if (!outFile.fail())    outFile.close(); 
	return prevCol[len2];
}


struct levenshtein_distance_operator_mod {

public:
     levenshtein_distance_operator_mod(double _search_distance = 0,bool _mod=false/*,std::string _filename="levenstein.txt"*/):search_distance(_search_distance),
             mod(_mod)/*,filename(_filename)*/ {  }

     double operator()( const std::string &a,  const std::string &b) {
     std::string str1 = prepare_field_mod<std::string>(a,-1);
     std::string str2 = prepare_field_mod<std::string>(b,-1);
     double minlen = (double) std::min(str1.size(), str2.size());
     

 

     double distance = levenshtein_distance_mod<std::string>(str1, str2, search_distance/*,filename*/,mod);  // http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance

     if (distance == 0 ) return  distance;

     
     return       std::min(1.,distance/minlen);

  }
private:
	double search_distance;
        //std::string filename;
        bool mod;
        
};


#endif
