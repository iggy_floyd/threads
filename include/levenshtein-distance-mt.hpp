#ifndef LEVENSHTEIN_MT_H
#define	LEVENSHTEIN_MT_H

#include <vector>
#include <string>
#include <sstream>
#include <algorithm>



#include "tools/tools-text.hpp"
#include "threadpool.hpp"
#include <boost/smart_ptr.hpp>
#include <boost/bind.hpp>


/****h* BK-TREE/levenshtein-distance
  *  NAME
  *    levenshtein-distance
  *  
  *  COPYRIGHT
  *    "(c) 2014 by <Unister Gmbh/Igor Marfin>" 
  *  
  *  SYNOPSIS
  *    
  *
  *
  *  DESCRIPTION
  *     calculates the Levenshtein Distance (LD)
  *    * "original" LD (see  http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance)
  *    * or so called similarity based on the "inverse LD"
  *
  *  INPUTS
  *
  *  RESULT
  *
  *  EXAMPLE
  *
  *  NOTES
  *    
  *  BUGS
  *  SEE ALSO
  *    BK-TREE/httpserver
  ******
  * You can use this space for remarks that should not be included
  * in the documentation.
  */



#include <chrono>
#include <thread>

//
// Helpers
boost::mutex m_io_monitor;


void print(std::string text)
{
  boost::mutex::scoped_lock lock(m_io_monitor);
  std::cout << text;
}


boost::mutex mutex;

template<class T>
class prepare_field_mt {
    public:
        prepare_field_mt(T s):_s(s) {}
        T get_transformed()  { return _s_transformed;}
        T get()  { return _s;}
        void run() { 
            const T delim("|||");
            _s_transformed = tools::boostsplit<T>(_s, delim)[0];
        }
        
    private:
        T _s;
        T _s_transformed;
                
};

/*
template<class T>
T prepare_field( const T &s11)
{



    // a new format: use '|||' delimiter             
         const T delim("|||");
         return tools::boostsplit<T>(s11, delim)[0];
}
*/




template<class T>
class levenstein_calculator_mt {
    public:
        levenstein_calculator_mt(std::vector<T> & col, std::vector<T> & prevCol,T i,T j, const char & s1, const char & s2):_col(col), 
                _prevCol(prevCol), _i(i),_j(j),_s1(s1),_s2(s2)
        {}
        void run() { 
/*            std::ostringstream str;
            str<<"_j:"<<_j<<" _i:"<<_i<<" _col[_j+1]: "<<_col[_j+1]<<" _prevCol[_j]:"<<_prevCol[_j]<<" _s1:"<<_s1<<" _s2:"<<_s2<<"\n";
            str.flush();
            print(str.str());
 */
            boost::unique_lock< boost::mutex > lock( mutex );
            _col[_j+1] = std::min( std::min(_prevCol[1 + _j] + 1, _col[_j] + 1),_prevCol[_j] + (_s1==_s2 ? 0 : 1) );
            //std::this_thread::sleep_for(std::chrono::milliseconds(10)); // 1ms calculation
        }
        
    private:
        std::vector<T> & _col;
        std::vector<T> & _prevCol;        
        T _i;
        T _j;
       const  char & _s1;
       const  char & _s2;
            
                
};

template<class T>
unsigned int levenshtein_distance_mt( const T &s1, const T & s2,  boost::threadpool::pool & tp) {
	const size_t len1 = s1.size(), len2 = s2.size();
	std::vector<unsigned int> col(len2+1), prevCol(len2+1);
    

       typedef boost::shared_ptr<levenstein_calculator_mt<unsigned int>> workerPtr;
       //boost::threadpool::pool tp(20); // tp is handle to the pool
  
       	for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;

        
       	for (unsigned int i = 0; i < len1; i++) {
		col[0] = i+1;
		for (unsigned int j = 0; j < len2; j++) {
//                    std::cout<<"j:"<<j<<" i:"<<i<<" col[_j+1]: "<< col[j+1]<<" prevCol[j]:"<<prevCol[j]<<" s1:"<< s1[i]<<" s2:"<< s2[j]<<"\n";
                    workerPtr job(new levenstein_calculator_mt<unsigned int>(col,prevCol,i,j,s1[i],s2[j]));      
                    boost::threadpool::schedule(tp, boost::threadpool::prio_task_func(10000-(i+j),boost::bind(&levenstein_calculator_mt<unsigned int>::run, job)));
                }
                tp.wait();
                col.swap(prevCol);
        }

       
        /*
	for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
	for (unsigned int i = 0; i < len1; i++) {
		col[0] = i+1;
		for (unsigned int j = 0; j < len2; j++)
			col[j+1] = std::min( std::min(prevCol[1 + j] + 1, col[j] + 1),
								prevCol[j] + (s1[i]==s2[j] ? 0 : 1) );
		col.swap(prevCol);
	}
          
         */
        
        
//       tp.wait();
       return prevCol[len2];
       return 0;
}






struct levenshtein_distance_operator_mt {
  double operator()( const std::string &a,  const std::string &b, boost::threadpool::pool & tp) {

      
//     std::string str1 = prepare_field<std::string>(a);
//     std::string str2 = prepare_field<std::string>(b);
      std::string str1;
      std::string str2;
      
// using threads, perform the transformation of the fields       
//      {
        typedef boost::shared_ptr<prepare_field_mt<std::string>> workerPtr;
//        boost::threadpool::pool tp(2); // tp is handle to the pool
        
        // Add tasks
      workerPtr job1(new prepare_field_mt<std::string>(a));      
      workerPtr job2(new prepare_field_mt<std::string>(b));      
      boost::threadpool::schedule(tp, boost::bind(&prepare_field_mt<std::string>::run, job1));
      boost::threadpool::schedule(tp, boost::bind(&prepare_field_mt<std::string>::run, job2));
      
        tp.wait();
        str1 = job1->get_transformed();
        str2 = job2->get_transformed();        
  //    }
      
      

     double distance = levenshtein_distance_mt<std::string>(str1, str2,tp);  // http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance

     if (distance == 0 ) return  distance;
     double minlen = std::min(str1.size(), str2.size());
     return       std::min(1.,distance/minlen);

  }
};


#endif