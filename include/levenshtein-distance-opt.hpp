#ifndef LEVENSHTEIN_OPT_H
#define	LEVENSHTEIN_OPT_H

#include <vector>
#include <string>
#include <sstream>
#include <algorithm>

#include <cmath>


#include "tools/tools-text.hpp"


/****h* BK-TREE/levenshtein-distance
  *  NAME
  *    levenshtein-distance
  *  
  *  COPYRIGHT
  *    "(c) 2014 by <Unister Gmbh/Igor Marfin>" 
  *  
  *  SYNOPSIS
  *    
  *
  *
  *  DESCRIPTION
  *     calculates the Levenshtein Distance (LD)
  *    * "original" LD (see  http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance)
  *    * or so called similarity based on the "inverse LD"
  *
  *  INPUTS
  *
  *  RESULT
  *
  *  EXAMPLE
  *
  *  NOTES
  *    
  *  BUGS
  *  SEE ALSO
  *    BK-TREE/httpserver
  ******
  * You can use this space for remarks that should not be included
  * in the documentation.
  */






template<class T>
T prepare_field( const T &s11)
{



    // a new format: use '|||' delimiter             
         const T delim("|||");
         return tools::boostsplit<T>(s11, delim)[0];
}





template<class T>
unsigned int levenshtein_distance_opt( const T &s1, const T & s2) {
	const size_t len1 = s1.size(), len2 = s2.size();
	std::vector<unsigned int> col(len2+1), prevCol(len2+1);

	for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
	for (unsigned int i = 0; i < len1; i++) {
		col[0] = i+1;
		for (unsigned int j = 0; j < len2; j++)
			col[j+1] = std::min( std::min(prevCol[1 + j] + 1, col[j] + 1),
								prevCol[j] + (s1[i]==s2[j] ? 0 : 1) );
		col.swap(prevCol);
	}
	return prevCol[len2];
}



// optimized levenshtein distance calculator

struct levenshtein_distance_operator_opt {
  double operator()( const std::string &a,  const std::string &b) {

      
     std::string str1 = prepare_field<std::string>(a);
     std::string str2 = prepare_field<std::string>(b);
      
     // here the optimization  goes 
     // preparation
     std::string internal_delim("-");
     std::string email_delim("@");
     std::vector<std::string> subfields1=tools::boostsplit<std::string>(str1,internal_delim);
     std::vector<std::string> subfields2=tools::boostsplit<std::string>(str2,internal_delim);
     
     bool _isemail1 = tools::boostsplit<std::string>(str1,email_delim).size()>1;
     bool _isemail2 = tools::boostsplit<std::string>(str2,email_delim).size()>1;
     bool _isbothemails = _isemail1 == _isemail2;
     

     double distance = 0;
          
     
     if ( (subfields1.size() != subfields2.size()) || (subfields1.size() == 0) || !_isbothemails || _isemail1) {
         
         distance = levenshtein_distance_opt<std::string>(str1, str2);  // http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance
         if (distance == 0 ) return  distance;
         double minlen = std::min(str1.size(), str2.size());
         return       std::min(1.,distance/minlen);

     }
     // optimization comes
     else {
          
         for (size_t i=0; i<subfields1.size(); i++) {
             
             double minlen = std::min(subfields1[i].size(), subfields2[i].size());
             double tmpdistance=std::min(1.,levenshtein_distance_opt<std::string>(subfields1[i], subfields2[i])/minlen);
             
             std::cout<<"temporary distance "<< tmpdistance<<"\n";
             
             distance +=    tmpdistance*tmpdistance;
         }
          distance /= subfields1.size();
          distance = std::sqrt(distance);
          return distance;
         
     }
     

  }
};


#endif