/* 
 * File:   thread_pool.h
 * Author: debian
 *
 * Created on December 8, 2014, 1:08 PM
 */

#ifndef THREAD_POOL_H
#define	THREAD_POOL_H

#include <queue>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

class thread_pool
{
private:
  std::queue< boost::function< void() > > tasks_;
  boost::thread_group threads_;
  std::size_t available_;
  boost::mutex mutex_;
  boost::condition_variable condition_;
  boost::condition_variable condition2_;
  bool running_;
  bool all_ready_;
  std::size_t initial_res_;
public:

  /// @brief Constructor.
  thread_pool( std::size_t pool_size )
    : available_( pool_size ),
      running_( true ),all_ready_(true), initial_res_(pool_size)
  {
    for ( std::size_t i = 0; i < pool_size; ++i )
    {
      threads_.create_thread( boost::bind( &thread_pool::pool_main, this ) ) ;
    }
  }

  /// @brief Destructor.
  ~thread_pool()
  {
    // Set running flag to false then notify all threads.
    {
//           std::cout<<"killing all tasks \n";  
      boost::unique_lock< boost::mutex > lock( mutex_ );
      while ( !all_ready_)
      {
//        std::cout<<"waiting to delete \n";
        condition2_.wait( lock );
      }
      running_ = false;
      condition_.notify_all();      
      lock.unlock();
      
    }

    try
    {
      threads_.join_all();
    }
    // Suppress all exceptions.
    catch ( ... ) {}
       //std::cout<<"killed  \n";  
  }

  /// @brief Add task to the thread pool if a thread is currently available.
  template < typename Task >
  bool run_task( Task task )
  {
    boost::unique_lock< boost::mutex > lock( mutex_ );

    // If no threads are available, then return.
    if ( 0 == available_ ) return false;

    // Decrement count, indicating thread is no longer available.
    --available_;

    // Set task and signal condition variable so that a worker thread will
    // wake up andl use the task.
    tasks_.push( boost::function< void() >( task ) );
    all_ready_= false;
    condition_.notify_one();
    
    return true;
  }

  void wait() {
      boost::unique_lock< boost::mutex > lock( mutex_ );
      all_ready_= false;
      condition2_.notify_one();  
    
  }
  
private:
  /// @brief Entry point for pool threads.
  void pool_main()
  {
    while( running_ )
    {
      // Wait on condition variable while the task is empty and the pool is
      // still running.
      boost::unique_lock< boost::mutex > lock( mutex_ );
      while ( tasks_.empty() && running_ )
      {
          //std::cout<<"lock taks \n";
        condition_.wait( lock );
      }
      // If pool is no longer running, break out.
      if ( !running_ ) break;

      // Copy task locally and remove from the queue.  This is done within
      // its own scope so that the task object is destructed immediately
      // after running the task.  This is useful in the event that the
      // function contains shared_ptr arguments bound via bind.
      {
        boost::function< void() > task = tasks_.front();
        tasks_.pop();

        lock.unlock();

        // Run the task.
        try
        {
          task();
        }
        // Suppress all exceptions.
        catch ( ... ) {}
      }

      // Task has finished, so increment count of available threads.
      lock.lock();
      ++available_;
      
       
        if (available_ == initial_res_) {
//        std::cout<<"all are ready \n";
        all_ready_= true;
       condition2_.notify_one();
      
    } else {
        all_ready_= false;
      condition2_.notify_one();
    }
    //std::cout<<"finishing taks \n";
      
      
    } // while running_
    

    
  }
};

#endif	/* THREAD_POOL_H */

