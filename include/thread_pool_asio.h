/* 
 * File:   thread_pool_asio.h
 * Author: debian
 *
 * Created on December 8, 2014, 12:50 PM
 */

#ifndef THREAD_POOL_ASIO_H
#define	THREAD_POOL_ASIO_H


#include <boost/asio.hpp>
#include <boost/ref.hpp>
#include <boost/thread.hpp>
#include "boost/function.hpp" 
#include "boost/bind.hpp" 

class thread_pool_asio
{

    private:
  boost::asio::io_service io_service_;
  boost::asio::io_service::work work_;
  boost::thread_group threads_;
  std::size_t available_; 
  boost::mutex mutex_;
  bool all_ready_;
  std::size_t initial_res_;
  boost::condition_variable condition_;
  
public:

  /// @brief Constructor.
  thread_pool_asio( std::size_t pool_size )
    : work_( io_service_ ),
      available_( pool_size ), all_ready_(true), initial_res_(pool_size)
  {
    for ( std::size_t i = 0; i < pool_size; ++i )
    {
//      threads_.create_thread( boost::bind( &boost::asio::io_service::run,
//                                           &io_service_ ) );
        boost::thread *t = new boost::thread(boost::bind( &boost::asio::io_service::run,
                                           &io_service_ ) ); 
        threads_.add_thread(t);

    }
  }

  /// @brief Destructor.
  ~thread_pool_asio()
  {
      boost::unique_lock< boost::mutex > lock( mutex_ );
      while ( !all_ready_)
      {
//          std::cout<<"waiting to delete \n";
        condition_.wait( lock );
      }
      lock.unlock();
      
    // Force all threads to return from io_service::run().
   io_service_.stop();

    // Suppress all exceptions.
    try
    {
    //   std::cout<<"killing all tasks \n";  
      threads_.join_all();
   
    }
    catch ( ... ) {/*std::cout<<"not killed all tasks \n";  */ }
  //  std::cout<<"killed  \n";  
  }

  /// @brief Adds a task to the thread pool if a thread is currently available.
  template < typename Task >
  bool run_task( Task  task )
  {
    boost::unique_lock< boost::mutex > lock( mutex_ );

    // If no threads are available, then return.
    if ( 0 == available_ ) return false;

    // Decrement count, indicating thread is no longer available.
    --available_;
    
    
    // Post a wrapped task into the queue.
    io_service_.post( boost::bind( &thread_pool_asio::wrap_task, this,
                                   boost::function< void() >( task ) ) );
    
   all_ready_= false;
   condition_.notify_one();

   return true;
  }
  
  void wait() {
      boost::unique_lock< boost::mutex > lock( mutex_ );
      all_ready_= false;
      condition_.notify_one();  
    
  }

private:
  /// @brief Wrap a task so that the available count can be increased once
  ///        the user provided task has completed.
  void wrap_task( boost::function< void() > task )
  {
    // Run the user supplied task.
    try
    {
//      std::cout<<"doing taks \n";
      task();
    }
    // Suppress all exceptions.
    catch ( ... ) {}

    // Task has finished, so increment count of available threads.
    boost::unique_lock< boost::mutex > lock( mutex_ );
    ++available_;
    if (available_ == initial_res_) {
//        std::cout<<"all are ready \n";
        all_ready_= true;
       condition_.notify_one();
      
    } else {
        all_ready_= false;
      condition_.notify_one();
    }
//    std::cout<<"finishing taks \n";

  }
  
};

#endif	/* THREAD_POOL_ASIO_H */

