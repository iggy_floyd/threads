#!/bin/bash

  program=`basename \`pwd\``  
  sqlite3_include="-I../sqlite3"
  soci_include=" -I../soci/src/core -I../soci/src/backends/sqlite3 "

  [[ `which mysql_config` ]] && soci_include_mysql="-DSOCIMYSQL=1  -I/usr/include/mysql -I../soci/src/backends/mysql"

  for i in src/*.cc; do g++ -g -Iinclude ${sqlite3_include} ${soci_include} ${soci_include_mysql}  -c ${i}; done
  ar cru lib${program}.a *.o
  ranlib lib${program}.a
  mv  lib${program}.a lib

