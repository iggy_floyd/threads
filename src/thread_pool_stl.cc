#include "thread_pool_stl.h"



using namespace std;
thread_pool_stl::thread_pool_stl(unsigned int num_threads) : num_threads(num_threads){
task_mutex.lock();
init_threads();
task_mutex.unlock();
}
thread_pool_stl::~thread_pool_stl(){
    
    
task_mutex.lock();
join = true;
task_mutex.unlock();

if (_nojoin)  {
    for(auto i = threads.begin();i != threads.end();i++)
    i->detach();
    return;
}
    
    

for(auto i = threads.begin();i != threads.end();i++)
i->join();
}
void thread_pool_stl::init_threads(){
for(int i = 0;i < num_threads;i++){
std::function<void(void)> f = std::bind(&thread_pool_stl::thread_func, this);
threads.push_back(std::move(std::thread(f)));
}
}
void thread_pool_stl::thread_func(){
for(;;){
task_mutex.lock();
if(tasks.empty() && !join){
task_mutex.unlock();
this_thread::yield();
continue;
}
else if(!tasks.empty()){
auto f = std::move(tasks.front());
tasks.pop_front();
task_mutex.unlock();
f.get();
}
else if(join){
task_mutex.unlock();
return;
}
else if(tasks.empty() && _cleartasks){
task_mutex.unlock();
return;
}
}
}


void thread_pool_stl::emergency_close() {
    task_mutex.lock();
    join = true;
    task_mutex.unlock();
    
   for(auto i = threads.begin();i != threads.end();i++)
    i->detach();
    
    threads.clear();
    
    return ;
    }


void thread_pool_stl::noJoin(bool mode) {
    task_mutex.lock();
    _nojoin = mode;
    task_mutex.unlock();
    
    return ;
    }



void thread_pool_stl::cleartasks(bool mode) {
    task_mutex.lock();
    _cleartasks = mode;
        tasks.clear();
    task_mutex.unlock();
}