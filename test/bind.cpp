
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>

#include "thread_pool_asio.h"
#include "tools/cpu-time.hpp"

using namespace std;


boost::mutex mutex;
int calls = 0;

void worker( int  i, int j, int & sum ) {  
//	std::cout<<"before sum="<<sum<<"\n";  

        boost::unique_lock< boost::mutex > lock( mutex );
	sum += i+j; 
        calls++;
        lock.unlock();	

//	std::cout<<"sum="<<sum<<" i="<<i<<" j="<<j<<" sum="<<sum<<"\n";
}
void work( int & sum ) { sum+=5;};

const std::string  test()
{


std::ostringstream ss;
std::locale l("de_DE.UTF-8");


 
  int sum =0;

  boost::function< void() > workFun;
  workFun=boost::bind(work,boost::ref(sum));
  workFun();
  boost::function< void() > workFun2;
  workFun2=boost::bind(worker, 1,2,boost::ref(sum));
  workFun2();


ss<<sum<<"\n";
return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(bind)
{
using namespace boost::python;
def("test", test);

}



