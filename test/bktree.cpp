
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>

#include <boost/thread/mutex.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"



#include "bk-tree-mt.hpp"
#include "bk-tree.hpp"


#include <chrono>
#include <thread>

using namespace std;
using namespace boost::threadpool;








const std::string  test()
{


std::ostringstream ss;
std::locale l("de_DE.UTF-8");


trees::bktree_mt<std::string, double, levenshtein_distance_operator_v2> bktree_mt;
trees::bktree<std::string, double, levenshtein_distance_operator_v2> bktree;

boost::threadpool::pool * tp_ptr = new boost::threadpool::pool(20);
bktree_mt.settp(tp_ptr);

string str1 = "igor-marfin-zeuthenersrt-15732|||1_ID|||traveller";
string str2 = "igro-marfin-zeuthenersrt-15742|||2_ID|||traveller";
string str3 = "igr-marfin-zeuthenersrt-15752|||3_ID|||traveller";

bktree_mt.insert(str1);
bktree_mt.insert(str2);


bktree.insert(str1);
bktree.insert(str2);


double wall01,wall02,wall03,wall04;

wall01=tools::get_wall_time();
auto result = bktree_mt.find_within(str3,1.-0.2,false,true); // get with similarity of 0.2
wall02=tools::get_wall_time();
ss<<wall02-wall01<<"\n";



std::transform(result.begin(), result.end(), std::ostream_iterator<std::string>(ss, "\n"),[] (std::pair<std::string, double> &p) {
                     std::ostringstream str;
                     str<<p.first<<" with "<<p.second;
                      return str.str();
               });


wall03=tools::get_wall_time();
result = bktree.find_within(str3,1.-0.2,false,true); // get with similarity of 0.2
wall04=tools::get_wall_time();
ss<<wall04-wall03<<"\n";

std::transform(result.begin(), result.end(), std::ostream_iterator<std::string>(ss, "\n"),[] (std::pair<std::string, double> &p) {
                     std::ostringstream str;
                     str<<p.first<<" with "<<p.second;
                      return str.str();
               });

               
delete tp_ptr;

return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(bktree)
{
using namespace boost::python;
def("test", test);

}



