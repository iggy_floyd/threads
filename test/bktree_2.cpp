
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include <fstream>
#include  <locale>

#include <boost/thread/mutex.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"



#include "bk-tree-mt.hpp"
#include "bk-tree.hpp"


#include <chrono>
#include <thread>

using namespace std;
using namespace boost::threadpool;






 void gen_random(char *s, const int len) {
     for (int i = 0; i < len; ++i) {
         int randomChar = rand()%(26+26+10);
         if (randomChar < 26)
             s[i] = 'a' + randomChar;
         else if (randomChar < 26+26)
             s[i] = 'A' + randomChar - 26;
         else
             s[i] = '0' + randomChar - 26 - 26;
     }
     s[len] = 0;
 }





const std::string  test()
{



std::ostringstream ss;
std::locale l("de_DE.UTF-8");

//std::ifstream file("./BookerNames10K.txt");
//std::ifstream file("./BookerNames100K.txt");
std::ifstream file("./BookerNames1M.txt");

trees::bktree_mt<std::string, double, levenshtein_distance_operator_v2> bktree_mt;
trees::bktree<std::string, double, levenshtein_distance_operator_v2> bktree;
std::vector<std::string> tosearch;
std::vector<std::string> tosearch_orig;

if (file.is_open() && !file.eof() ) {
    
    std::string line;     
    getline(file, line);
    if (!line.empty()) {
        // random string
        int sz=10;   
        char aa[sz+1];
        gen_random(aa,sz);
    
        bktree_mt.insert(line+std::string(aa));    
        bktree.insert(line+std::string(aa));
        tosearch.push_back(line+std::string(aa));
         tosearch_orig.push_back(line);
    }
    while (!file.eof()) {
      getline(file, line);
    if (!line.empty()) {
     
        // random string
        int sz=10;   
        char aa[sz+1];
        gen_random(aa,sz);
    
        bktree_mt.insert(line+std::string(aa));    
        bktree.insert(line+std::string(aa));
        tosearch.push_back(line+std::string(aa));
         tosearch_orig.push_back(line);

    }
      
    }
    file.close();
    std::cout<<"read has been finished\n";
}




boost::threadpool::pool * tp_ptr = new boost::threadpool::pool();
tp_ptr->size_controller().resize(8);
bktree_mt.settp(tp_ptr);



double wall01,wall02,wall03,wall04,dt_wall01,dt_wall02;
dt_wall01=0;
dt_wall02=0;
int count =0;

for (int i =tosearch.size()-1 ;i>=tosearch.size()-100;i--) {
    std::cout<<i<<"\n";
    std::string srch = (i%1000 == 0)?tosearch[i]:tosearch_orig[i];
    std::cout<<srch<<"\n";
    wall01=tools::get_wall_time();    
//auto result = bktree_mt.find_within(srch,1.-0.80,false,true); // get with similarity of 0.80
auto result = bktree.find_within(srch,1.-0.80,false,true); // get with similarity of 0.80
wall02=tools::get_wall_time();
dt_wall01 += wall02 - wall01;
if (result.size()>0) count+=1;

//ss<<result.size()<<"\n";
/*std::transform(result.begin(), result.end(), std::ostream_iterator<std::string>(ss, "\n"),[] (std::pair<std::string, double> &p) {
                     std::ostringstream str;
                     str<<p.first<<" with "<<p.second;
                      return str.str();
               });
*/
}
ss<<count<<"\n";




/*

std::transform(result.begin(), result.end(), std::ostream_iterator<std::string>(ss, "\n"),[] (std::pair<std::string, double> &p) {
                     std::ostringstream str;
                     str<<p.first<<" with "<<p.second;
                      return str.str();
               });
*/

ss<<dt_wall01<<"\n";

//bktree.dump_tree();
               
delete tp_ptr;

return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(bktree_2)
{
using namespace boost::python;
def("test", test);

}



