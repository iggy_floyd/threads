
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include <fstream>
#include  <locale>

#include <boost/thread/mutex.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"



#include "bk-tree-mt.hpp"
#include "bk-tree.hpp"
#include "bk-tree-container.h"


#include <chrono>
#include <thread>

using namespace std;
using namespace boost::threadpool;






 void gen_random(char *s, const int len) {
     for (int i = 0; i < len; ++i) {
         int randomChar = rand()%(26+26+10);
         if (randomChar < 26)
             s[i] = 'a' + randomChar;
         else if (randomChar < 26+26)
             s[i] = 'A' + randomChar - 26;
         else
             s[i] = '0' + randomChar - 26 - 26;
     }
     s[len] = 0;
 }





const std::string  test()
{



std::ostringstream ss;
std::locale l("de_DE.UTF-8");

//std::ifstream file("./BookerNames10K.txt");
//std::ifstream file("./BookerNames100K.txt");
std::ifstream file("./BookerNames1M.txt");
std::ifstream file2("./BookerNames1M.txt");
std::ifstream file3("./BookerNames1M.txt");

//typedef trees::bktree_mt<std::string, double, levenshtein_distance_operator_v2> datatype;
typedef trees::bktree<std::string, double, levenshtein_distance_operator_v2> datatype;

trees::bk_tree_container<datatype,std::string,double> bktreecontainer(7000); 
//trees::bk_tree_container<datatype,std::string,double> bktreecontainer;

std::vector<std::string> tosearch;
std::vector<std::string> tosearch_orig;

if (file.is_open() && !file.eof() ) {
    
    std::string line;     
    getline(file, line);
    if (!line.empty()) {
        // random string
        int sz=10;   
        char aa[sz+1];
        gen_random(aa,sz);
    
//        bktree_mt.insert(line+std::string(aa));    
//        bktree.insert(line+std::string(aa));
        bktreecontainer.insert(line+std::string(aa));
        tosearch.push_back(line+std::string(aa));
        tosearch_orig.push_back(line);
        
    }
    while (!file.eof()) {
      getline(file, line);
    if (!line.empty()) {
     
        // random string
        int sz=10;   
        char aa[sz+1];
        gen_random(aa,sz);
    
//        bktree_mt.insert(line+std::string(aa));    
//        bktree.insert(line+std::string(aa));
        bktreecontainer.insert(line+std::string(aa));
        tosearch.push_back(line+std::string(aa));
        tosearch_orig.push_back(line);

    }
      
    }
    file.close();
    std::cout<<"read has been finished\n";
}



if (file2.is_open() && !file2.eof() ) {
    
    std::string line;     
    getline(file2, line);
    if (!line.empty()) {
        // random string
        int sz=10;   
        char aa[sz+1];
        gen_random(aa,sz);
    
//        bktree_mt.insert(line+std::string(aa));    
//        bktree.insert(line+std::string(aa));
        bktreecontainer.insert(line+std::string(aa));
        
    }
    while (!file2.eof()) {
      getline(file2, line);
    if (!line.empty()) {
     
        // random string
        int sz=10;   
        char aa[sz+1];
        gen_random(aa,sz);
    
//        bktree_mt.insert(line+std::string(aa));    
//        bktree.insert(line+std::string(aa));
        bktreecontainer.insert(line+std::string(aa));

    }
      
    }
    file2.close();
    std::cout<<"read has been finished\n";
}

if (file3.is_open() && !file3.eof() ) {
    
    std::string line;     
    getline(file3, line);
    if (!line.empty()) {
        // random string
        int sz=10;   
        char aa[sz+1];
        gen_random(aa,sz);
    
//        bktree_mt.insert(line+std::string(aa));    
//        bktree.insert(line+std::string(aa));
        bktreecontainer.insert(line+std::string(aa));
        
    }
    while (!file3.eof()) {
      getline(file3, line);
    if (!line.empty()) {
     
        // random string
        int sz=10;   
        char aa[sz+1];
        gen_random(aa,sz);
    
//        bktree_mt.insert(line+std::string(aa));    
//        bktree.insert(line+std::string(aa));
        bktreecontainer.insert(line+std::string(aa));

    }
      
    }
    file3.close();
    std::cout<<"read has been finished\n";
}



double wall01,wall02,wall03,wall04,dt_wall01,dt_wall02;
dt_wall01=0;
dt_wall02=0;
int count =0;
//thread_pool_stl * pool = new thread_pool_stl(8);
thread_pool_stl * pool = nullptr;
 boost::threadpool::pool * tp_ptr = new boost::threadpool::pool(8);
 //boost::threadpool::pool * tp_ptr2 = new boost::threadpool::pool(20);
for (int i =tosearch.size()-1 ;i>=tosearch.size()-100;i--) {
    std::cout<<i<<"\n";
    std::string srch = (i%100 == 0)?tosearch[i]:tosearch_orig[i];
    std::cout<<srch<<"\n";
    tp_ptr->clear();
    wall01=tools::get_wall_time();    
//auto result = bktreecontainer.find_within(pool,tosearch[i],1.-0.80,false,true); // get with similarity of 0.80
//auto result = bktreecontainer.find_within(tp_ptr,tp_ptr2,tosearch[i],1.-0.80,false,true); // get with similarity of 0.80
auto result = bktreecontainer.find_within(tp_ptr,srch,1.-0.80,false,true); // get with similarity of 0.80
wall02=tools::get_wall_time();

std::transform(result.begin(), result.end(), std::ostream_iterator<std::string>(std::cout, "\n"),[] (std::pair<std::string, double> &p) {
                     std::ostringstream str;
                     str<<p.first<<" with "<<p.second;
                      return str.str();
               });

dt_wall01 += wall02 - wall01;
if (result.size()>0) count+=1;


}

//delete pool;
 delete tp_ptr;
// delete tp_ptr2;
ss<<count<<"\n";
ss<<dt_wall01<<"\n";



return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(bktree_3)
{
using namespace boost::python;
def("test", test);

}



