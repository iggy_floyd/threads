
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include <fstream>
#include  <locale>

#include <boost/thread/mutex.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"



#include "bk-tree-mt.hpp"
#include "bk-tree.hpp"
#include "bktree-container.h"


#include <chrono>
#include <thread>

using namespace std;
using namespace boost::threadpool;






 void gen_random(char *s, const int len) {
     for (int i = 0; i < len; ++i) {
         int randomChar = rand()%(26+26+10);
         if (randomChar < 26)
             s[i] = 'a' + randomChar;
         else if (randomChar < 26+26)
             s[i] = 'A' + randomChar - 26;
         else
             s[i] = '0' + randomChar - 26 - 26;
     }
     s[len] = 0;
 }





const std::string  test()
{



std::ostringstream ss;
std::locale l("de_DE.UTF-8");

std::ifstream file("./BookerNames10K.txt");
//std::ifstream file("./BookerNames100K.txt");
//std::ifstream file("./BookerNames1M.txt");
//std::ifstream file2("./BookerNames1M.txt");
//std::ifstream file3("./BookerNames1M.txt");


typedef trees::bktree<std::string, double, levenshtein_distance_operator_v2> datatype;

//trees::bk_tree_container<datatype,std::string,double> bktreecontainer(7000); 
trees::bktree_container<datatype,std::string,double> bktreecontainer(10); 


std::vector<std::string> tosearch;
std::vector<std::string> tosearch_orig;

if (file.is_open() && !file.eof() ) {
    
    std::string line;     
    getline(file, line);
    if (!line.empty()) {
        // random string
        int sz=10;   
        char aa[sz+1];
        gen_random(aa,sz);
    

        bktreecontainer.insert(line+std::string(aa));
        tosearch.push_back(line+std::string(aa));
        tosearch_orig.push_back(line);
        
    }
    while (!file.eof()) {
      getline(file, line);
    if (!line.empty()) {
     
        // random string
        int sz=10;   
        char aa[sz+1];
        gen_random(aa,sz);
    
        bktreecontainer.insert(line+std::string(aa));
        tosearch.push_back(line+std::string(aa));
        tosearch_orig.push_back(line);

    }
      
    }
    file.close();
    std::cout<<"read has been finished\n";
}


std::cout<<"find ... "<<tosearch.back()<<"\n";
bktreecontainer.find_within(tosearch.back(),1.-0.80,false,true);

bool empty = bktreecontainer.empty();
int  active = bktreecontainer.active();
int pending = bktreecontainer.pending();
std::cout<<"should we wait ? Our status is ... "<<empty<<"  " <<active<<" "<<pending<<"\n";

bktreecontainer.wait();
bktreecontainer.clean(); 

auto results = bktreecontainer.getResults();

for (auto& p : results) {
    if (p.second.size()>0)
                std::cout<<p.first<<" found to be connected to "<<p.second[0].first <<" with "<< p.second[0].second <<"\n";
    else        std::cout<<p.first<<" is not connected " <<"\n";
            }




return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(bktree_4)
{
using namespace boost::python;
def("test", test);

}



