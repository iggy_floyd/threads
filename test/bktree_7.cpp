
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <map>
#include  <cstring>
#include  <sstream>
#include <fstream>
#include  <locale>

#include <boost/thread/mutex.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"



#include "bk-tree-mt.hpp"
#include "bk-tree.hpp"
#include "bktree-container.h"
#include "bktree-container.h"

#include <chrono>
#include <thread>

using namespace std;
using namespace boost::threadpool;






 void gen_random(char *s, const int len) {
     for (int i = 0; i < len; ++i) {
         int randomChar = rand()%(26+26+10);
         if (randomChar < 26)
             s[i] = 'a' + randomChar;
         else if (randomChar < 26+26)
             s[i] = 'A' + randomChar - 26;
         else
             s[i] = '0' + randomChar - 26 - 26;
     }
     s[len] = 0;
 }





const std::string  test()
{



std::ostringstream ss;
std::locale l("de_DE.UTF-8");

std::ifstream file("./BookerNames10K.txt");
//std::ifstream file("./BookerNames100K.txt");

// data to be processed: 3M entries
//std::ifstream file("./BookerNames1M.txt");
//std::ifstream file2("./BookerNames1M.txt");
//std::ifstream file3("./BookerNames1M.txt");

/*
 *  some parameters of this test
 *   
 *  rate_of_exactness --  its inverse defines how oft the property should be found the container
 * 
 *  delay     --   in the real life,  there is a delay (it might be 10 ms or 1s or 10s  or ...) between "search calls"
 * 
 */

const double rate_of_exactness = 0.005; // only one among 200 properties should be found in the container

// assume that  we have a delay of 200 ms on average. Depends on different factors: insert time in the tree, communication with Cayley, 
// other code in the communicator.cpp,  rules (Double Booking, Overall) calculation, php background etc
size_t delay = 0; 


trees::bktree<std::string, double, levenshtein_distance_operator_v2> bktree;
bktree.insert(std::string(128,'#'));




typedef trees::bktree<std::string, double, levenshtein_distance_operator_v2> datatype;
trees::bktree_container<datatype,std::string,double> bktreecontainer(8); 


bktreecontainer.insert(std::string(128,'#'));

string str1 = "igor-marfin-zeuthenersrt-15732|||1_ID|||traveller";
string str2 = "igro-marfin-zeuthenersrt-15742|||2_ID|||traveller";


std::vector<std::string> tosearch;


if (file.is_open() && !file.eof() ) {
    
    std::string line;     
    getline(file, line);
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];        
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    

        bktreecontainer.insert(std::string(bb)+"-"+line+"-"+std::string(aa));
        bktree.insert(std::string(bb)+"-"+line+"-"+std::string(aa));
        tosearch.push_back((std::string(bb)+"-"+line+"-"+std::string(aa)));
    }
    while (!file.eof()) {
      getline(file, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
    
        bktreecontainer.insert(std::string(bb)+"-"+line+"-"+std::string(aa));
        bktree.insert(std::string(bb)+"-"+line+"-"+std::string(aa));
        tosearch.push_back((std::string(bb)+"-"+line+"-"+std::string(aa)));
        

    }
      
    }
    file.close();
    std::cout<<"read has been finished\n";
}







for (int i=0;i<20;i++) {
    std::cout<<"!!!delete ..."<<tosearch[i]<<"\n";
    bktreecontainer.remove(tosearch[i]); 
}






for (int i=0;i<30;i++) {
    std::cout<<"find ..."<<tosearch[i]<<"\n";
    bktreecontainer.find_within(tosearch[i],1.-0.80,false,true); // get with similarity of 0.80
}

// wait until active and pending jobs finish. In the real life, it will not happen, because we don't need the results back

bktreecontainer.wait(); 
// prepare the results
bktreecontainer.clean();
int count=0;
auto results = bktreecontainer.getResults();

std::cout<<"\n\n\n";

for (auto& p : results) {
    if (p.second.size()>0) {
        count++;
                std::cout<<p.first<<" found to be connected to "<<p.second[0].first <<" with "<< p.second[0].second <<"\n";
    }
    else        std::cout<<p.first<<" is not connected " <<"\n";
            }
std::cout<<"\nTotal connected properties ... "<<count<<"\n";

 

return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(bktree_7)
{
using namespace boost::python;
def("test", test);

}



