char const* test()
{
return "hello, world";
}

#include <boost/python.hpp>
BOOST_PYTHON_MODULE(hello)
{
using namespace boost::python;
def("test", test);
}




/*

to create *.so file do

g++  -I/usr/include/python2.7 -I/usr/local/include  -fpic  hello.cpp -shared -lboost_python -o hello.so

*/
