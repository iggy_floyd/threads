
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>

#include <boost/thread/mutex.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"

#include "levenshtein-distance-mt.hpp"
#include "levenshtein-distance.hpp"

#include <chrono>
#include <thread>

using namespace std;
using namespace boost::threadpool;



int calls = 0;




const std::string  test()
{


std::ostringstream ss;
std::locale l("de_DE.UTF-8");

double wall01,wall02,wall03,wall04;


levenshtein_distance_operator_v2 ld;
levenshtein_distance_operator_mt ld_mt;
//string str1 = "igor-marfin-zeuthenersrt-15732|||1_ID|||traveller";
//string str2 = "igro-marfin-zeuthenersrt-15742|||2_ID|||traveller";


string str1 = "iafdadasdarwrwrsdfas-fdsdfsdfsd-fsdsd-sdfsd|||1_ID|||traveller";
string str2 = "sdfsdfsdfdsf4534-6756867867867-sdfsfc-fsdsdfsd-df|||2_ID|||traveller";


double distance;
int numelements_in_bk_tree=1;

wall01=tools::get_wall_time();
for (int i=0;i<numelements_in_bk_tree;i++)  distance=ld(str1,str2);
wall02=tools::get_wall_time();

ss<<distance<<"\n";
ss<<wall02-wall01<<"\n";



boost::threadpool::pool tp(10);
wall03=tools::get_wall_time();
for (int i=0;i<numelements_in_bk_tree;i++) distance=ld_mt(str1,str2,tp);
wall04=tools::get_wall_time();
ss<<distance<<"\n";
ss<<wall04-wall03<<"\n";


return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(levenshtein)
{
using namespace boost::python;
def("test", test);

}



