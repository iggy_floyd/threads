
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>

#include <boost/thread/mutex.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"

#include "levenshtein-distance-opt.hpp"
#include "levenshtein-distance.hpp"

#include <chrono>
#include <thread>

using namespace std;




int calls = 0;




const std::string  test()
{


std::ostringstream ss;
std::locale l("de_DE.UTF-8");

double wall01,wall02;


levenshtein_distance_operator_v2 ld;
levenshtein_distance_operator_opt ld_opt;
string str1 = "igor-marfin-zeuthenersrt-15732|||1_ID|||traveller";
string str2 = "igro-marfin-zeuthenersrt-15742|||2_ID|||traveller";



//str1 = "iafdadasdarwrwrsdfas-fdsdfsdfsd-fsdsd|||1_ID|||traveller"; //log strings
//str2 = "sdfsdfsdfdsf4534-67568678677-sdfsfc|||2_ID|||traveller";


double distance;
int numelements_in_bk_tree=3000000; // 3M
numelements_in_bk_tree=100000; // 100K
numelements_in_bk_tree=1; // 1

double dt=0;


for (int i=0;i<numelements_in_bk_tree;i++)  {    
    wall01=tools::get_wall_time(); 
    distance=ld(str1,str2);
    wall02=tools::get_wall_time();
    dt += wall02-wall01;
}


ss<<distance<<"\n";
ss<<dt<<"\n";




dt=0;
for (int i=0;i<numelements_in_bk_tree;i++) 
 {    
    wall01=tools::get_wall_time(); 
    distance=ld_opt(str1,str2);
    wall02=tools::get_wall_time();
    dt += wall02-wall01;
}

ss<<distance<<"\n";
ss<<dt<<"\n";


return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(levenshtein_2)
{
using namespace boost::python;
def("test", test);

}



