
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>

#include <boost/thread/mutex.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"


#include "levenshtein-distance.hpp"

#include <chrono>
#include <thread>
#include "bk-tree.hpp"
using namespace std;




int calls = 0;




const std::string  test()
{


std::ostringstream ss;
std::locale l("de_DE.UTF-8");

double wall01,wall02;


levenshtein_distance_operator_v2 ld;
std::vector<std::string> vec;
trees::bktree<std::string, double, levenshtein_distance_operator_v2> bktree;

vec.push_back("1234567890"); //0
vec.push_back("1234");//1
vec.push_back("12354");//2
vec.push_back("12353");//3
vec.push_back("12352");//4
vec.push_back("12344");//5
vec.push_back("123444");//7
vec.push_back("123559");//6

        

for (int i=0;i<vec.size();i++) {
    bktree.insert(vec[i]);
    for (int j=i+1;j<vec.size();j++){
        std::cout<<"vec(i)="<<vec[i]<<" vec(j)="<<vec[j]<<" ld="<<ld(vec[i],vec[j])<<"\n";
    }
}


bktree.dump_tree();


std::cout<<"\n\n";
 std::cout<<"vec(4)="<<vec[4]<<" vec(2)="<<vec[2]<<" ld="<<ld(vec[4],vec[2])<<"\n";
 std::cout<<"vec(3)="<<vec[3]<<" vec(5)="<<vec[5]<<" ld="<<ld(vec[3],vec[5])<<"\n";
 std::cout<<"vec(2)="<<vec[2]<<" vec(3)="<<vec[3]<<" ld="<<ld(vec[2],vec[3])<<"\n";
 std::cout<<"vec(0)="<<vec[0]<<" vec(1)="<<vec[1]<<" ld="<<ld(vec[0],vec[1])<<"\n";
 std::cout<<"vec(0)="<<vec[0]<<" vec(2)="<<vec[2]<<" ld="<<ld(vec[0],vec[2])<<"\n";











return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(levenshtein_3)
{
using namespace boost::python;
def("test", test);

}



