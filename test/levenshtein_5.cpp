
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>




#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"
#include "tools/random_transformation_string.h"


#include "levenshtein-distance.hpp"

#include <chrono>
#include <thread>

using namespace std;



int calls = 0;




const std::string  test()
{


std::ostringstream ss;
std::locale l("de_DE.UTF-8");

double wall01,wall02,wall03,wall04;


levenshtein_distance_operator_v2 ld;

//string str1 = "igor-marfin-zeuthenersrt-15732|||1_ID|||traveller";
//string str2 = "igor-marfin-zeuthenersrt-15732|||2_ID|||traveller";

string str1 = "igorboris-marfinzypla-zeuthenersrtasse-157325|||1_ID|||traveller";


int count =0,count15=0, count20=0,num=10000;
double dist_max = 0.15;
double avg_dist=0.;
for (size_t t=0;t<num; t++) {
    std::cout<<"transformation "<<t<<"\n******************\n\n";
    std::string str2 = tools::random_transformation_string(str1,dist_max).generate();
    std::cout<<str2<<"\n";
    double dist = ld(str1,str2);
    avg_dist += dist;
     std::cout<<dist<<"\n";
     
     if (dist <=dist_max) count++;
     if (dist <=1.5*dist_max) count15++;
     if (dist <=2.*dist_max) count20++;
             
             
    
    std::cout<<"******************\n\n";
}


std::cout<<"count = "<<count<<" "<<((double)count/num)*100<<" (%)\n";
std::cout<<"count15 = "<<count15<<" "<<((double)count15/num)*100<<" (%)\n";
std::cout<<"count20 = "<<count20<<" "<<((double)count20/num)*100<<" (%)\n";
std::cout<<"average distance = "<<avg_dist/num<<" at nominal max distance"<<dist_max<<" \n";

return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(levenshtein_5)
{
using namespace boost::python;
def("test", test);

}



