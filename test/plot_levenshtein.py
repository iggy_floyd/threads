import pylab

list_of_files=[("levenstein.txt","not-correlated")]
datalist = [ ( pylab.loadtxt(filename), label ) for filename, label in list_of_files ]

for data, label in datalist:
    pylab.plot( data[:,0], data[:,1], label=label )
pylab.ylim([0,1])

pylab.legend()
pylab.title("Comparison of the Levenshtein")
pylab.xlabel("Num.of the  Letter")
pylab.ylabel("Total Similarity")
pylab.show()
