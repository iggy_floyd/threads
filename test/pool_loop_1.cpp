
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>

#include "thread_pool_asio.h"
#include "tools/cpu-time.hpp"

using namespace std;


  boost::mutex mutex;
  int calls = 0;

void worker( int  i, int j, int & sum ) {  

        boost::unique_lock< boost::mutex > lock( mutex );
	sum += i+j; 
        calls++;
        lock.unlock();	

}


const std::string  test()
{


std::ostringstream ss;
std::locale l("de_DE.UTF-8");


 
// create a pool of the workers

  thread_pool_asio*  ptrPool =  new thread_pool_asio( 20 );
  int sum =0;

double wall01,wall02,wall03,wall04;
wall01=tools::get_wall_time();
for (int i=0;i<100;i++)
  for (int j=0;j<100;j++) {

	worker(i,j,sum);

}

wall02 = tools::get_wall_time();

 ss<<sum<<"\n";
// ss<<wall02-wall01<<"\n";
 ss<<calls<<"\n";



sum=0;
calls = 0;

wall03=tools::get_wall_time();
for (int i=0;i<100;i++)
  for (int j=0;j<100;j++) {

	boost::function< void() > _func;	
	_func = boost::bind(worker, i,j,boost::ref(sum) );
     while ( !ptrPool->run_task( boost::bind(worker, i,j,boost::ref(sum) )) ) {
		ptrPool->wait(); 
	} 
}






delete ptrPool;

wall04 = tools::get_wall_time();

 ss<<sum<<"\n";
// ss<<wall04-wall03<<"\n";
 ss<<calls<<"\n";





return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(pool_loop_1)
{
using namespace boost::python;
def("test", test);

}



