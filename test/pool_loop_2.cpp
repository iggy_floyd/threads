
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>

#include "thread_pool.h"
#include "tools/cpu-time.hpp"
#include <chrono>
#include <thread>

using namespace std;


  boost::mutex mutex;
  int calls = 0;

void worker( int  i, int j, int & sum ) {  

     
//        sleep(1); // 1000ms calculation
      std::this_thread::sleep_for(std::chrono::milliseconds(1)); // 1ms calculation
      int tmp = i+j;

        // critical section to be locked: return result        
        boost::unique_lock< boost::mutex > lock( mutex );
	sum += tmp; 
        calls++;        
//        std::cout<<"sum="<<sum<<" i="<<i<<" j="<<j<<" sum="<<sum<<"\n";
        lock.unlock();	

}


const std::string  test()
{


std::ostringstream ss;
std::locale l("de_DE.UTF-8");


 
// create a pool of the workers

  thread_pool*  ptrPool =  new thread_pool( 20 );
  int sum =0;

double wall01,wall02,wall03,wall04;
wall01=tools::get_wall_time();
for (int i=0;i<100;i++)
  for (int j=0;j<100;j++) {

	worker(i,j,sum);

}

wall02 = tools::get_wall_time();

 ss<<sum<<"\n";
 ss<<wall02-wall01<<"\n";
 ss<<calls<<"\n";



sum=0;
calls = 0;

wall03=tools::get_wall_time();
for (int i=0;i<100;i++)
  for (int j=0;j<100;j++) {

	boost::function< void() > _func;	
	_func = boost::bind(worker, i,j,boost::ref(sum) );
     while ( !ptrPool->run_task( boost::bind(worker, i,j,boost::ref(sum) )) ) {
		ptrPool->wait(); 

	} 

}






delete ptrPool;

wall04 = tools::get_wall_time();

 ss<<sum<<"\n";
 ss<<wall04-wall03<<"\n";
 ss<<calls<<"\n";





return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(pool_loop_2)
{
using namespace boost::python;
def("test", test);

}



