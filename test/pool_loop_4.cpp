
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>

#include <boost/thread/mutex.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/bind.hpp>
#include <threadpool.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"
#include <chrono>
#include <thread>

using namespace std;

using namespace boost::threadpool;


  boost::mutex mutex;
  int calls = 0;


class worker2 {

public: 
	worker2(int i, int j): _i(i),_j(j) {}
	~worker2() {}

	void run() {

			std::this_thread::sleep_for(std::chrono::milliseconds(1)); // 1ms calculation
			result=tools::vprintf("%d",_i+_j);
			boost::unique_lock< boost::mutex > lock( mutex );
		        calls++;        

		}

	std::string get()  { return result;}

private:
	int _i;
	int _j;
	std::string result;

};

std::string worker( int  i, int j ) {  

     
      std::this_thread::sleep_for(std::chrono::milliseconds(1)); // 1ms calculation
      int tmp = i+j;
      

        // critical section to be locked: return result        
        boost::unique_lock< boost::mutex > lock( mutex );
        calls++;        
//        std::cout<<"sum="<<sum<<" i="<<i<<" j="<<j<<" sum="<<sum<<"\n";
        lock.unlock();	

	return tools::vprintf("%d",tmp);

}

std::string worker3() { return string();  }
const char * worker4() { return "Hi";  }
const char * worker5(int , int ) { return "Hi";  }

struct worker6 {

	const char * operator()() {return "Hi";}
        static const char * test() {return "Hi";}

};



const std::string  test()
{


std::ostringstream ss;
std::locale l("de_DE.UTF-8");


 






double wall01,wall02,wall03,wall04;
wall01=tools::get_wall_time();
for (int i=0;i<10;i++)
  for (int j=0;j<10;j++) {
        worker(i,j);
	//std::cout<<worker(i,j)<<"\n";

}


wall02 = tools::get_wall_time();

 
 ss<<wall02-wall01<<"\n";
 ss<<calls<<"\n";





calls = 0;
typedef boost::shared_ptr<worker2> workerPtr;


vector<workerPtr> result;

{
// create a pool of the workers
pool tp(20); // tp is handle to the pool


wall03=tools::get_wall_time();
for (int i=0;i<10;i++)
  for (int j=0;j<10;j++) {

//  boost::function<const char *(int, int)> _func;
//  _func = boost::bind(worker5, _1, _2);  
//   schedule(tp,_func); // doesn't work
//   schedule(tp,&worker3); // doesn't work
//   schedule(tp,&worker4); //work
//   schedule(tp,&worker5,i,j);     // doesn't work
//     future<const char*> res= schedule(tp,&(worker6::test)); // work

// Add tasks
      workerPtr job(new worker2(i,j));      
      schedule(tp, boost::bind(&worker2::run, job));
      result.push_back(job);

}

}

//for (auto it=result.begin();it!=result.end();it++)
//cout<<(*it)->get()<<"\n";




wall04 = tools::get_wall_time();


 ss<<wall04-wall03<<"\n";
 ss<<calls<<"\n";





return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(pool_loop_4)
{
using namespace boost::python;
def("test", test);

}



