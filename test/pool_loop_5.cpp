
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>

#include <boost/thread/mutex.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/bind.hpp>

//#include "ctpl/ctpl.h"
#include "thread_pool_stl.h"
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"
#include <chrono>
#include <thread>






boost::mutex mutex;
int calls = 0;


class worker2 {

public: 
	worker2(int i, int j): _i(i),_j(j) {}
	~worker2() {}

	void run() {

			std::this_thread::sleep_for(std::chrono::milliseconds(1)); // 1ms calculation
			result=tools::vprintf("%d",_i+_j);
			boost::unique_lock< boost::mutex > lock( mutex );
		        calls++;        

		}

	std::string get()  { return result;}

private:
	int _i;
	int _j;
	std::string result;

};

std::string worker(  int  i, int j ) {  

     
      std::this_thread::sleep_for(std::chrono::milliseconds(1)); // 1ms calculation
      int tmp = i+j;
      

        // critical section to be locked: return result        
        boost::unique_lock< boost::mutex > lock( mutex );
        calls++;        
//        std::cout<<"sum="<<sum<<" i="<<i<<" j="<<j<<" sum="<<sum<<"\n";
        lock.unlock();	

	return tools::vprintf("%d",tmp);

}



std::string worker4(int ) { return "Hi";  }

const std::string  test()
{


std::ostringstream ss;
std::locale l("de_DE.UTF-8");


 






double wall01,wall02,wall03,wall04;
wall01=tools::get_wall_time();
for (int i=0;i<40;i++)
  for (int j=0;j<40;j++) {
        worker(i,j);
	//std::cout<<worker(i,j)<<"\n";

}


wall02 = tools::get_wall_time();

 
 ss<<wall02-wall01<<"\n";
 ss<<calls<<"\n";






//typedef boost::shared_ptr<worker2> workerPtr;

//thread_pool_stl pool(20 /* two threads in the pool */);
//p.async(function<bool(int)>(func_bool_int), i)
//pool.async(std::function<pair<int, bool>(int)>(is_prime), i)

//int n=1;
//int m=1;

//pool.async(std::function<std::string(int,int)>(worker), std::move(n),std::move(m)); doesn't work
//pool.async(std::function<std::string (int)>(worker4), 1); //works

//std::function<std::string()> func = std::bind(worker, n,m);
//pool.async(func); //works
//std::function<void()> func = std::bind(&worker2::run, new worker2(n,m)); // works
 
calls = 0;
wall03=tools::get_wall_time();
{
    
    thread_pool_stl pool(8 );
    
    for (int i=0;i<40;i++)
    for (int j=0;j<40;j++) { 
        pool.async(std::function<std::string()>(std::bind(worker, i,j)));
                
    }
    
}
wall04 = tools::get_wall_time();




 ss<<wall04-wall03<<"\n";
 ss<<calls<<"\n";


 calls = 0;
wall03=tools::get_wall_time();
{
    std::vector<std::future<std::string>> f;
    thread_pool_stl pool(8 );
    
    for (int i=0;i<40;i++)
    for (int j=0;j<40;j++) { 
        f.emplace_back(pool.async(std::function<std::string()>(std::bind(worker, i,j))));
                
    }
/*
    for (auto it = f.begin(); it != f.end(); it++) {
        std::cout << it->get() << std::endl;
    }
*/
}
wall04 = tools::get_wall_time();



 ss<<wall04-wall03<<"\n";
 ss<<calls<<"\n";

calls = 0;
wall03=tools::get_wall_time();
typedef std::shared_ptr<worker2> workerPtr;
std::vector<workerPtr> result;
{
    //std::vector<std::future<std::string>> f;
    thread_pool_stl pool(4 );
    
    for (int i=0;i<40;i++)
    for (int j=0;j<40;j++) { 
         workerPtr job(new worker2(i,j)); 
         pool.async(std::function<void()>(std::bind(&worker2::run,job)));
        //f.emplace_back(pool.async(std::function<std::string()>(std::bind(worker, i,j))));
         result.push_back(job);
         
                
    }
    /*
    for (auto it = f.begin(); it != f.end(); it++) {
        std::cout << it->get() << std::endl;
    }
  */
}
wall04 = tools::get_wall_time();


for (auto it=result.begin();it!=result.end();it++)
std::cout<<(*it)->get()<<"\n";



 ss<<wall04-wall03<<"\n";
 ss<<calls<<"\n";

 
 calls = 0;
wall03=tools::get_wall_time();

    
    thread_pool_stl * pool_ptr = new  thread_pool_stl (8 );
    
    for (int i=0;i<40;i++)
    for (int j=0;j<40;j++) { 
        pool_ptr->async(std::function<std::string()>(std::bind(worker, i,j)));
                
    }
    
    delete pool_ptr;
wall04 = tools::get_wall_time();
 ss<<wall04-wall03<<"\n";
 ss<<calls<<"\n";
 

return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(pool_loop_5)
{
using namespace boost::python;
def("test", test);

}



