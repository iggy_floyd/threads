
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>

#include "thread_pool_asio.h"
#include "tools/cpu-time.hpp"

using namespace std;

/*
struct worker
{

  void operator()(int i, int j, int & sum) {
	
		sum += i+j;
	
	};

};
*/

  boost::mutex mutex;
  int calls = 0;

void worker( int  i, int j, int & sum ) {  
//	std::cout<<"before sum="<<sum<<"\n";  

        boost::unique_lock< boost::mutex > lock( mutex );
	sum += i+j; 
        calls++;
        lock.unlock();	

//	std::cout<<"sum="<<sum<<" i="<<i<<" j="<<j<<" sum="<<sum<<"\n";
}
void work( int & sum ) { sum+=5;};

const std::string  test()
{


std::ostringstream ss;
std::locale l("de_DE.UTF-8");


 
// create a pool of the workers

//  thread_pool_asio   pool( 10 );
  thread_pool_asio*  ptrPool =  new thread_pool_asio( 10 );
  int sum =0;
/*
  boost::function< void() > workFun;
  workFun=boost::bind(work,boost::ref(sum));
  workFun();
  boost::function< void() > workFun2;
  workFun2=boost::bind(worker, 1,2,boost::ref(sum));
  workFun2();

*/
 // pool.run_task( boost::bind(worker, 2,3,boost::ref(sum) )); 
 // pool.run_task( boost::bind(worker, 2,3,sum ));

// an attempt to calculate a part of the loop in parallel
/*
  for (int i=0;i<10000;i++)
  for (int j=0;j<10000;j++) {
	
   if (! pool.run_task( boost::bind(worker, i,j,boost::ref(sum) ) )) worker(i,j,sum);

}
*/
 
/*
 for (int i=1;i<3;i++) {
	std::cout<<"1 sum = "<<sum<<"\n";
//	pool.run_task( boost::bind(worker, i,i,boost::ref(sum) ));	
	ptrPool->run_task( boost::bind(worker, i,i,boost::ref(sum) ));	
	std::cout<<"2 sum = "<<sum<<"\n";

 }
 */

double wall01,wall02,wall03,wall04;
wall01=tools::get_wall_time();
for (int i=0;i<500;i++)
  for (int j=0;j<500;j++) {

	worker(i,j,sum);

}

wall02 = tools::get_wall_time();

 ss<<sum<<"\n";
 ss<<wall02-wall01<<"\n";
 ss<<calls<<"\n";



sum=0;
calls = 0;

wall03=tools::get_wall_time();
for (int i=0;i<500;i++)
  for (int j=0;j<500;j++) {

	boost::function< void() > _func;	
	_func = boost::bind(worker, i,j,boost::ref(sum) );
//   if (! ptrPool->run_task( boost::bind(worker, i,j,boost::ref(sum) ) )) worker(i,j,sum);
     while ( !ptrPool->run_task( boost::bind(worker, i,j,boost::ref(sum) )) ) {
//     while ( !ptrPool->run_task( _func ) ) {
//	 std::cout<<" !!!!!!!!!!! waiting for i= "<<i<<" j="<<j<<"\n"; 
//		ptrPool->wait(); 
	} 
//	std::cout<<" i="<<i<<" j="<<j<<" sum="<<sum<<"\n";

//	ptrPool->run_task( boost::bind(worker, i,j,boost::ref(sum) ));
//   if (! ptrPool->run_task( boost::bind(worker, i,j,boost::ref(sum) ) )) worker(i,j,sum);

}






delete ptrPool;
//std::cout<<"sleeping \n";
//sleep(2);

wall04 = tools::get_wall_time();

 ss<<sum<<"\n";
 ss<<wall04-wall03<<"\n";
 ss<<calls<<"\n";





return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(pool_loop_1)
{
using namespace boost::python;
def("test", test);

}



