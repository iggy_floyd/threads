#! /usr/bin/env python
# -*- coding: utf-8 -*-


'''
#  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
#  All rights reserved.
#
#
#  Usage: %(scriptName)s
#

'''


from subprocess import Popen,PIPE
import inspect
import sys


sys.path.append('.')



def test_cpp(file,incs="",args=""):

 cmd="python-config --cflags"
 cmd=cmd.split()
 (pyc,err) = Popen(cmd, stdout=PIPE).communicate()
 pyc=pyc.replace("-Wstrict-prototypes","");
 pyc=pyc.replace("\n","");
 cmd="g++  -std=c++0x  -I. %s   -I/usr/local/include 	   %s -fpic %s.cpp -shared -lboost_python %s -o %s.so"%(pyc,incs,file,args,file)
# print cmd
# return 
 cmd=cmd.split()
 (out,err) = Popen(cmd, stdout=PIPE).communicate()
 import importlib
 mymodule = importlib.import_module(file, package=file)
 
 return getattr(mymodule,"test")()

  




def hello():
   """  hello.cpp

   >>> hello()
   'hello, world'
   """
   return test_cpp(str(inspect.stack()[0][3]))
   





def bind():
   r"""  bind.cpp
         encode( '", \\, \\t, \\n' )
   >>> bind()
   8
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))



def pool_loop_1():
   r"""  pool_loop_1.cpp
         encode( '", \\, \\t, \\n' )
   >>> pool_loop_1()
   990000
   10000
   990000
   10000
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def pool_loop_2():
   r"""  pool_loop_2.cpp
         encode( '", \\, \\t, \\n' )
   >>> pool_loop_2()
   990000
   10000
   990000
   10000
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))



def threadpool_tutorial():
   r"""  threadpool_tutorial.cpp
         encode( '", \\, \\t, \\n' )
   >>> threadpool_tutorial()
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def pool_loop_3():
   r"""  pool_loop_3.cpp
         encode( '", \\, \\t, \\n' )
   >>> pool_loop_3()
   990000
   10000
   990000
   10000
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def pool_loop_4():
   r"""  pool_loop_4.cpp
         encode( '", \\, \\t, \\n' )
   >>> pool_loop_4()
   990000
   10000
   990000
   10000
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))

def pool_loop_5():
   r"""  pool_loop_5.cpp
         encode( '", \\, \\t, \\n' )
   >>> pool_loop_5()
   990000
   10000
   990000
   10000
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def levenshtein():
   r"""  levenshtein.cpp
         encode( '", \\, \\t, \\n' )
   >>> levenshtein()
   990000
   10000
   990000
   10000
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))



def levenshtein_2():
   r"""  levenshtein_2.cpp
         encode( '", \\, \\t, \\n' )
   >>> levenshtein_2()
   990000
   10000
   990000
   10000
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))

def levenshtein_3():
   r"""  levenshtein_3.cpp
         encode( '", \\, \\t, \\n' )
   >>> levenshtein_3()
   990000
   10000
   990000
   10000
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def levenshtein_4():
   r"""  levenshtein_4.cpp
         encode( '", \\, \\t, \\n' )
   >>> levenshtein_4()
   990000
   10000
   990000
   10000
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))

def levenshtein_5():
   r"""  levenshtein_5.cpp
         encode( '", \\, \\t, \\n' )
   >>> levenshtein_5()
   990000
   10000
   990000
   10000
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def levenshtein_6():
   r"""  levenshtein_6.cpp
         encode( '", \\, \\t, \\n' )
   >>> levenshtein_6()
   990000
   10000
   990000
   10000
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))
   import pylab
### plotting
   list_of_files=[("levenstein_similar.txt","similar"),("levenstein_long_differ.txt","long_differ"),
     ("levenstein_similar_differ.txt","similar_differ"),("levenstein_differ_similar.txt","differ_similar"),
     ("levenstein_differ_differ.txt","differ_differ"),("levenstein_equal.txt","equal"),("levenstein_smalldiff_equal.txt","smalldiff_equal")]
   datalist = [ ( pylab.loadtxt(filename), label ) for filename, label in list_of_files ]

   for data, label in datalist:
      pylab.plot( data[:,0], data[:,1], label=label )
   pylab.plot((0,1), (1,0), linestyle='-', marker='o',color="black",label="ideal")
   pylab.ylim([0,1.1])
   pylab.xlim([0,1.1])

   pylab.legend()
   pylab.title("Comparison of the Levenshtein")
   pylab.xlabel("Num.of the  Letter")
   pylab.ylabel("Total Similarity")
   pylab.savefig("levenstein_comparison.pdf",dpi=200)


def bktree():
   r"""  bktree.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree()
   igor-marfin-zeuthenersrt-15732|||1_ID|||traveller with 0.931034
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def bktree_2():
   r"""  bktree_2.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree_2()
   igor-marfin-zeuthenersrt-15732|||1_ID|||traveller with 0.931034
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def bktree_3():
   r"""  bktree_3.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree_3()
   igor-marfin-zeuthenersrt-15732|||1_ID|||traveller with 0.931034
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))



def bktree_4():
   r"""  bktree_4.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree_4()
   igor-marfin-zeuthenersrt-15732|||1_ID|||traveller with 0.931034
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))



def bktree_5():
   r"""  bktree_5.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree_5()
   igor-marfin-zeuthenersrt-15732|||1_ID|||traveller with 0.931034
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))



def bktree_6():
   r"""  bktree_6.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree_6()
   igor-marfin-zeuthenersrt-15732|||1_ID|||traveller with 0.931034
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def bktree_7():
   r"""  bktree_7.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree_7()
   igor-marfin-zeuthenersrt-15732|||1_ID|||traveller with 0.931034
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def bktree_8():
   r"""  bktree_8.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree_8()
   igor-marfin-zeuthenersrt-15732|||1_ID|||traveller with 0.931034
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def bktree_9():
   r"""  bktree_9.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree_9()
   igor-marfin-zeuthenersrt-15732|||1_ID|||traveller with 0.931034
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def bktree_10():
   r"""  bktree_10.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree_10()
   igor-marfin-zeuthenersrt-15732|||1_ID|||traveller with 0.931034
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))





if __name__ == '__main__':


    print __doc__ % {'scriptName' : sys.argv[0]}
  
    import doctest
    
#    doctest.testmod(verbose=True)
#    doctest.run_docstring_examples(bind, globals(),verbose=True)
#    doctest.run_docstring_examples(pool_loop_1, globals(),verbose=True)
#    doctest.run_docstring_examples(pool_loop_2, globals(),verbose=True)
#    doctest.run_docstring_examples(threadpool_tutorial, globals(),verbose=True)
#    doctest.run_docstring_examples(pool_loop_3, globals(),verbose=True)
#    doctest.run_docstring_examples(pool_loop_4, globals(),verbose=True)
#    doctest.run_docstring_examples(pool_loop_5, globals(),verbose=True)
#    doctest.run_docstring_examples(levenshtein, globals(),verbose=True)
#    doctest.run_docstring_examples(levenshtein_2, globals(),verbose=True)
#    doctest.run_docstring_examples(levenshtein_3, globals(),verbose=True)
#    doctest.run_docstring_examples(levenshtein_4, globals(),verbose=True)
#    doctest.run_docstring_examples(levenshtein_5, globals(),verbose=True)
    doctest.run_docstring_examples(levenshtein_6, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree_2, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree_3, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree_4, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree_5, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree_6, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree_7, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree_8, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree_9, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree_10, globals(),verbose=True)


    import os
    cmd='rm *.so *.db'
    os.system(cmd)

